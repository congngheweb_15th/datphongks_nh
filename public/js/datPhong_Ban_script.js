jQuery(document).ready(function ($) {

    $(".scroll").click(function (event) {
        event.preventDefault();
        $('html,body').animate({
            scrollTop: $(this.hash).offset().top
        }, 800, 'swing');
    });

});
// uniform
$(function () {
    $('input[type="checkbox"], input[type="radio"], select').uniform();
});

// social icon
$(document).ready(function ($) {
    $('.social i').tooltip('hide')
});

// 

var wow = new WOW({
    boxClass: 'wowload', // animated element css class (default is wow)
    animateClass: 'animated', // animation css class (default is animated)
    offset: 0, // distance to the element when triggering the animation (default is 0)
    mobile: true, // trigger animations on mobile devices (default is true)
    live: true // act on asynchronously loaded content (default is true)
});
wow.init();


function toDate(dateStr) {
    const [day, month, year] = dateStr.split("-")
    return new Date(year, month - 1, day)
}

function formatNumber(nStr, decSeperate, groupSeperate) {
    nStr += '';
    x = nStr.split(decSeperate);
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + groupSeperate + '$2');
    }
    return x1 + x2;
}		

//Load ajax số phòng từ loại phòng
$(document).ready(function(){

    // Đặt phòng
    $('#id_loaiphong').on('change', function(e){

        var idLoaiPhong = e.target.value;
        //ajax lấy số phòng
        $.get('datphong/ajax-phong?idLoaiPhong=' + idLoaiPhong, function(data){
            $('#id_soPhong').empty();
            $.each(data, function(index, phong){
                $('#id_soPhong').append('<option value="' + phong.id + '">' + phong.soPhong + '</option>');
            });
        });

        //ajax lấy hinh anh minh hoa
        $.get('datphong/ajax-hinhanhminhhoa?idLoaiPhong=' + idLoaiPhong, function(data){
            $.each(data, function(i, val){
                $('#hinhAnhMinhHoa').attr('src', 'public/uploads/images/'+ val.hinhAnhMinhHoa);
                
                var ngayNhan = $('#NgayNhanPhong').val();
                var ngayTra = $('#NgayTraPhong').val();
                var soNgay = (toDate(ngayTra)-toDate(ngayNhan))/(1000 * 60 * 60 * 24);
                var tongTien = (soNgay+1)*val.giaLoaiPhong;
                var dinhDang = formatNumber(tongTien, '.', ',');
                $('#tien').html(dinhDang);
                $('#tongTien').val(tongTien);
            });
        });
         
    });
    // Đặt bàn
    $('#id_loaiBan').on('change', function(e){
        
        var idLoaiBan = e.target.value;
        $.get('datban/ajax-hinhanhminhhoa?idLoaiBan=' + idLoaiBan, function(data){
            $.each(data, function(i, val){
                $('#hinhAnhMinhHoa').attr('src', 'public/uploads/images/'+ val.hinhAnhMinhHoa);
                var slBan = $('#slBan').val();
                var tongTien = slBan *  val.donGiaLoaiBan;
                var dinhDang = formatNumber(tongTien, '.', ',');
                $('#tien').html(dinhDang);
                $('#tongTien').val(tongTien);

            });
        });
            
    });
});

