// Hiển thị modal xem chi tiết
function formatDate(ngay){
    var tmp = new Date(ngay);  
    return tmp.getDate() + "-" + (tmp.getMonth()+1) + "-" + tmp.getFullYear(); // trả về định dạng dd-mm-YYYY
}

function formatNumber(nStr, decSeperate, groupSeperate) {
    nStr += '';
    x = nStr.split(decSeperate);
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + groupSeperate + '$2');
    }
    return x1 + x2;
}		


function getTT_DatPhong( ttdatphong ) {
    
    $('#id').html(ttdatphong.id);
    $('#hoTen').html(ttdatphong.khachdat.hoTen);
    $('#cmnd').html(ttdatphong.khachdat.cmnd);
    $('#sdt').html(ttdatphong.khachdat.sdt);
    $('#email').html(ttdatphong.khachdat.email);
    $('#ngayNhanPhong').html(formatDate(ttdatphong.ngayNhanPhong));
    $('#ngayTraPhong').html(formatDate(ttdatphong.ngayTraPhong));
    $('#soPhong').html(ttdatphong.phong.soPhong);
    $('#soNguoiLon').html(ttdatphong.soNguoiLon);
    $('#soTreEm').html(ttdatphong.soTreEm);
    $('#tongTien').html(formatNumber(ttdatphong.tongTien, '.', ',')+ " VND");
    
    
}


function getTT_DatBan( ttdatban ) {
    
    $('#id').html(ttdatban.id);
    $('#hoTen').html(ttdatban.khachdat.hoTen);
    $('#cmnd').html(ttdatban.khachdat.cmnd);
    $('#sdt').html(ttdatban.khachdat.sdt);
    $('#email').html(ttdatban.khachdat.email);
    //------------------------------------------------------//
    $('#thoiGianNhan').html(ttdatban.gioNhan +" | " + formatDate(ttdatban.ngayNhan));
    $('#slKhach').html(ttdatban.slKhach);
    $('#slBan').html(ttdatban.slBan);
    $('#diaDiem').html(ttdatban.diaDiemDatBan);
    $('#ghiChu').html(ttdatban.ghiChu);
    $('#tongTien').html(formatNumber(ttdatban.tongTien, '.', ',') + " VND");
    
}