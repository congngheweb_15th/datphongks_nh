/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 100128
 Source Host           : localhost:3306
 Source Schema         : datphongks_nh

 Target Server Type    : MySQL
 Target Server Version : 100128
 File Encoding         : 65001

 Date: 14/12/2017 16:29:58
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (1, '2014_10_12_000000_create_users_table', 1);
INSERT INTO `migrations` VALUES (2, '2014_10_12_100000_create_password_resets_table', 1);
INSERT INTO `migrations` VALUES (3, '2017_11_11_150333_create_khachdat_table', 1);
INSERT INTO `migrations` VALUES (4, '2017_11_11_150439_create_loaiphong_table', 1);
INSERT INTO `migrations` VALUES (5, '2017_11_11_150857_create_phong_table', 1);
INSERT INTO `migrations` VALUES (6, '2017_11_11_151007_create_datphong_table', 1);
INSERT INTO `migrations` VALUES (7, '2017_11_11_151041_create_loaiban_table', 1);
INSERT INTO `migrations` VALUES (8, '2017_11_11_151105_create_datban_table', 1);

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets`  (
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `password_resets_email_index`(`email`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for tbl_datban
-- ----------------------------
DROP TABLE IF EXISTS `tbl_datban`;
CREATE TABLE `tbl_datban`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_loaiBan` int(10) UNSIGNED NOT NULL,
  `id_khachDatBan` int(10) UNSIGNED NOT NULL,
  `slKhach` int(11) NOT NULL,
  `slBan` int(11) NOT NULL,
  `tongTien` int(11) NOT NULL,
  `ngayNhan` date NOT NULL,
  `gioNhan` time(0) NOT NULL,
  `diaDiemDatBan` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ghiChu` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `trangThai` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `tbl_datban_id_loaiban_foreign`(`id_loaiBan`) USING BTREE,
  INDEX `tbl_datban_id_khachdatban_foreign`(`id_khachDatBan`) USING BTREE,
  CONSTRAINT `tbl_datban_id_khachdatban_foreign` FOREIGN KEY (`id_khachDatBan`) REFERENCES `tbl_khachdat` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `tbl_datban_id_loaiban_foreign` FOREIGN KEY (`id_loaiBan`) REFERENCES `tbl_loaiban` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_datban
-- ----------------------------
INSERT INTO `tbl_datban` VALUES (1, 3, 2, 4, 1, 1700000, '2017-12-15', '17:00:00', 'Sân thượng', NULL, 3, '2017-12-14 01:56:21', '2017-12-14 02:01:26');

-- ----------------------------
-- Table structure for tbl_datphong
-- ----------------------------
DROP TABLE IF EXISTS `tbl_datphong`;
CREATE TABLE `tbl_datphong`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_khachDatPhong` int(10) UNSIGNED NOT NULL,
  `ngayNhanPhong` date NOT NULL,
  `ngayTraPhong` date NOT NULL,
  `id_soPhong` int(10) UNSIGNED NOT NULL,
  `soNguoiLon` int(11) NOT NULL,
  `soTreEm` int(11) NOT NULL,
  `tongTien` int(11) NOT NULL,
  `trangThai` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `tbl_datphong_id_sophong_foreign`(`id_soPhong`) USING BTREE,
  INDEX `tbl_datphong_id_khachdatphong_foreign`(`id_khachDatPhong`) USING BTREE,
  CONSTRAINT `tbl_datphong_id_khachdatphong_foreign` FOREIGN KEY (`id_khachDatPhong`) REFERENCES `tbl_khachdat` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `tbl_datphong_id_sophong_foreign` FOREIGN KEY (`id_soPhong`) REFERENCES `tbl_phong` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_datphong
-- ----------------------------
INSERT INTO `tbl_datphong` VALUES (1, 1, '2017-12-16', '2017-12-18', 1, 2, 1, 10500000, 3, '2017-12-14 01:45:24', '2017-12-14 01:53:51');

-- ----------------------------
-- Table structure for tbl_khachdat
-- ----------------------------
DROP TABLE IF EXISTS `tbl_khachdat`;
CREATE TABLE `tbl_khachdat`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `hoTen` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `cmnd` int(11) NOT NULL,
  `sdt` int(11) NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_khachdat
-- ----------------------------
INSERT INTO `tbl_khachdat` VALUES (1, 'Đoàn Thanh Nhân', 354637284, 984756382, 'dthan@gmail.com', '2017-12-14 01:45:24', '2017-12-14 01:45:24');
INSERT INTO `tbl_khachdat` VALUES (2, 'Trần Tuyết Hương', 367584973, 984756473, 'tthuong@gmail.com', '2017-12-14 01:56:21', '2017-12-14 01:56:21');

-- ----------------------------
-- Table structure for tbl_loaiban
-- ----------------------------
DROP TABLE IF EXISTS `tbl_loaiban`;
CREATE TABLE `tbl_loaiban`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tenLoaiBan` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `hinhAnhMinhHoa` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `moTa` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `donGiaLoaiBan` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_loaiban
-- ----------------------------
INSERT INTO `tbl_loaiban` VALUES (1, 'Bàn tròn', 'bantron.jpg', '- Tối đa: 10 người/bàn. Đơn giá đã bao gồm: phí phục vụ và trang trí bàn.', 1800000, '2017-12-14 02:18:53', '2017-12-14 02:24:49');
INSERT INTO `tbl_loaiban` VALUES (2, 'Bàn dài', 'bandai.gif', '- Tối đa: 12 người/bàn. Đơn giá đã bao gồm: phí phục vụ và trang trí bàn.', 2000000, '2017-12-14 02:19:32', '2017-12-14 02:25:02');
INSERT INTO `tbl_loaiban` VALUES (3, 'Bàn 4 người', 'ban4.png', '- Tối đa: 4 người/bàn. Đơn giá đã bao gồm: phí phục vụ và trang trí bàn.', 1700000, '2017-12-14 02:19:54', '2017-12-14 02:25:11');
INSERT INTO `tbl_loaiban` VALUES (4, 'Bàn 2 người', 'ban2.jpg', '- Tối đa: 2 người/bàn. Đơn giá đã bao gồm: phí phục vụ và trang trí bàn.', 1500000, '2017-12-14 02:20:11', '2017-12-14 02:25:20');

-- ----------------------------
-- Table structure for tbl_loaiphong
-- ----------------------------
DROP TABLE IF EXISTS `tbl_loaiphong`;
CREATE TABLE `tbl_loaiphong`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tenLoaiPhong` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `giaLoaiPhong` int(11) NOT NULL,
  `hinhAnhMinhHoa` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `moTa` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_loaiphong
-- ----------------------------
INSERT INTO `tbl_loaiphong` VALUES (3, 'Double Room', 3500000, 'doubleroom.jpg', 'Loại phòng Double Room bao gồm: 2 giường đôi, máy lạnh, TV, tủ lạnh mini,  truyền hình cáp, wifi, máy nước nóng-lạnh.', '2017-12-14 02:06:57', '2017-12-14 02:16:47');
INSERT INTO `tbl_loaiphong` VALUES (4, 'Twin Room', 2800000, 'twinroom.jpg', 'Loại phòng Twin Room bao gồm: 2 giường đơn,  máy lạnh, TV, tủ lạnh mini, truyền hình cáp, Wifi, máy nước nóng-lạnh.', '2017-12-14 02:08:04', '2017-12-14 02:23:21');
INSERT INTO `tbl_loaiphong` VALUES (5, 'Single Room', 2500000, 'singleroom.jpg', 'Loại phòng Single Room bao gồm: 1 giường đơn, máy lạnh, TV, tủ lạnh mini, truyền hình cáp, Wifi,máy nước nóng-lạnh.', '2017-12-14 02:09:11', '2017-12-14 02:24:02');
INSERT INTO `tbl_loaiphong` VALUES (6, 'Family Room', 3200000, 'familyroom.jpg', 'Loại phòng Family Room bao gồm: 1 giường đôi và 1 giường đơn, máy lạnh, TV, tủ lạnh mini, truyền hình cáp, Wifi, máy nước nóng-lạnh.', '2017-12-14 02:10:14', '2017-12-14 02:24:31');

-- ----------------------------
-- Table structure for tbl_phong
-- ----------------------------
DROP TABLE IF EXISTS `tbl_phong`;
CREATE TABLE `tbl_phong`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `soPhong` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_loaiphong` int(10) UNSIGNED NOT NULL,
  `trangThai` int(11) NOT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `tbl_phong_id_loaiphong_foreign`(`id_loaiphong`) USING BTREE,
  CONSTRAINT `tbl_phong_id_loaiphong_foreign` FOREIGN KEY (`id_loaiphong`) REFERENCES `tbl_loaiphong` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_phong
-- ----------------------------
INSERT INTO `tbl_phong` VALUES (1, 'DR100', 3, 1, '2017-12-14 02:11:31', '2017-12-14 01:45:23');
INSERT INTO `tbl_phong` VALUES (2, 'DR101', 3, 0, '2017-12-14 02:11:37', '2017-12-14 02:11:37');
INSERT INTO `tbl_phong` VALUES (3, 'DR102', 3, 0, '2017-12-14 02:11:42', '2017-12-14 02:11:42');
INSERT INTO `tbl_phong` VALUES (4, 'DR103', 3, 0, '2017-12-14 02:11:51', '2017-12-14 02:11:51');
INSERT INTO `tbl_phong` VALUES (5, 'DR104', 3, 0, '2017-12-14 02:11:56', '2017-12-14 02:11:56');
INSERT INTO `tbl_phong` VALUES (6, 'TR100', 4, 0, '2017-12-14 02:12:11', '2017-12-14 02:12:11');
INSERT INTO `tbl_phong` VALUES (7, 'TR101', 4, 0, '2017-12-14 02:12:16', '2017-12-14 02:12:16');
INSERT INTO `tbl_phong` VALUES (8, 'TR102', 4, 0, '2017-12-14 02:12:23', '2017-12-14 02:12:23');
INSERT INTO `tbl_phong` VALUES (9, 'TR103', 4, 0, '2017-12-14 02:12:27', '2017-12-14 02:12:27');
INSERT INTO `tbl_phong` VALUES (10, 'TR104', 4, 0, '2017-12-14 02:12:32', '2017-12-14 02:12:32');
INSERT INTO `tbl_phong` VALUES (11, 'SR100', 5, 0, '2017-12-14 02:12:41', '2017-12-14 02:12:41');
INSERT INTO `tbl_phong` VALUES (12, 'SR101', 5, 0, '2017-12-14 02:12:45', '2017-12-14 02:12:45');
INSERT INTO `tbl_phong` VALUES (13, 'SR102', 5, 0, '2017-12-14 02:12:49', '2017-12-14 02:12:49');
INSERT INTO `tbl_phong` VALUES (14, 'SR103', 5, 0, '2017-12-14 02:12:54', '2017-12-14 02:12:54');
INSERT INTO `tbl_phong` VALUES (15, 'SR104', 5, 0, '2017-12-14 02:12:59', '2017-12-14 02:12:59');
INSERT INTO `tbl_phong` VALUES (16, 'FR100', 6, 0, '2017-12-14 02:13:07', '2017-12-14 02:13:07');
INSERT INTO `tbl_phong` VALUES (17, 'FR101', 6, 0, '2017-12-14 02:13:13', '2017-12-14 02:13:13');
INSERT INTO `tbl_phong` VALUES (18, 'FR102', 6, 0, '2017-12-14 02:13:19', '2017-12-14 02:13:19');
INSERT INTO `tbl_phong` VALUES (19, 'FR103', 6, 0, '2017-12-14 02:13:23', '2017-12-14 02:13:23');
INSERT INTO `tbl_phong` VALUES (20, 'FR104', 6, 0, '2017-12-14 02:13:28', '2017-12-14 02:13:28');

-- ----------------------------
-- Table structure for tbl_users
-- ----------------------------
DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE `tbl_users`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phanQuyen` int(11) NOT NULL,
  `remember_token` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `tbl_users_email_unique`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_users
-- ----------------------------
INSERT INTO `tbl_users` VALUES (1, 'Administrator', 'admin@gmail.com', '$2y$10$yflPAKo1FVvdRzOBEt83fuFGhqAUe5tFXsKt0LNyBwHSVN9VsR2RG', 1, NULL, NULL, '2017-12-14 02:21:43');
INSERT INTO `tbl_users` VALUES (2, 'Kim Dương', 'phkduong@gmail.com', '$2y$10$z6LT0f0UzLmc/dMk0NUhZepBqZRWCsk/.otW6W74yeFOwt.E6ab2S', 2, NULL, '2017-12-14 02:21:28', '2017-12-14 02:21:28');

SET FOREIGN_KEY_CHECKS = 1;
