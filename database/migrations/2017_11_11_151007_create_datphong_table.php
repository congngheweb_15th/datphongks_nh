<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatphongTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_datphong', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('id_khachDatPhong')->unsigned();
            $table->date('ngayNhanPhong');
            $table->date('ngayTraPhong');
            $table->integer('id_soPhong')->unsigned();
            $table->integer('soNguoiLon');
            $table->integer('soTreEm');
            $table->integer('tongTien');
            $table->integer('trangThai');
            $table->timestamps();

            $table->foreign('id_soPhong')->references('id')->on('tbl_phong');
            $table->foreign('id_khachDatPhong')->references('id')->on('tbl_khachdat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_datphong');
    }
}
