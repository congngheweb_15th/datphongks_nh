<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatbanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_datban', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('id_loaiBan')->unsigned();
            $table->integer('id_khachDatBan')->unsigned();
            $table->integer('slKhach');
            $table->integer('slBan');
            $table->integer('tongTien');
            $table->date('ngayNhan');
            $table->time('gioNhan');
            $table->text('diaDiemDatBan')->collation('utf8_unicode_ci');
            $table->longtext('ghiChu')->collation('utf8_unicode_ci')->nullable();
            $table->integer('trangThai');
            $table->timestamps();
            
            $table->foreign('id_loaiBan')->references('id')->on('tbl_loaiban');
            $table->foreign('id_khachDatBan')->references('id')->on('tbl_khachdat');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_datban');
    }
}
