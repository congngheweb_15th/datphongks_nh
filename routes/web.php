<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('index');
})->name('trangChu');

Route::get('khachsan', 'loaiPhong_Controller@getDanhSachLoaiPhong');
Route::get('nhahang', 'loaiBanTiec_Controller@getDanhSachLoaiBan');
Route::get('gioithieu', function (){ return view('DatPhong_Ban.introduction'); });

Route::group(['prefix' => 'datphong'], function (){
    Route::get('/', 'datPhong_Controller@getViewDatPhong')->name('trangDatPhong');
    Route::post('/', 'datPhong_Controller@postDatPhong')->name('datPhong_post');
    Route::get('/thanhcong', function(){
        return view('DatPhong_Ban.DatPhong.datPhong_success');
    })->name('datPhong_success');

    //load Ajax phong
    Route::get('ajax-phong', 'datPhong_Controller@ajaxPhong');
    Route::get('ajax-hinhanhminhhoa', 'datPhong_Controller@ajaxHinhAnhMinhHoa');
    
});

Route::group(['prefix' => 'datban'], function (){
    Route::get('/', 'datBan_Controller@getViewDatBan')->name('trangDatBan');
    Route::post('/', 'datBan_Controller@postDatBan')->name('datBan_post');
    Route::get('/thanhcong', function(){
        return view('DatPhong_Ban.DatBan.datBan_success');
    })->name('datBan_success');
    Route::get('ajax-hinhanhminhhoa', 'datBan_Controller@getAjaxHinhAnh');
});

Route::get('/admin/dangnhap', 'nguoiDung_Controller@getDangNhap')->name('dangNhap_get');
Route::post('/dangnhap', 'nguoiDung_Controller@postDangNhap')->name('dangNhap_post');
Route::group(['prefix' => 'admin','middleware' => 'login'], function () {

    Route::get('/', 'admin_Controller@getIndex')->name('adminpage');

    Route::get('dangxuat', 'nguoiDung_Controller@getDangXuat')->name('dangXuat');
    Route::get('doimatkhau/{id}', 'nguoiDung_Controller@getDoiMatKhau')->name('doiMatKhau_get');
    Route::post('doimatkhau/{id}', 'nguoiDung_Controller@postDoiMatKhau')->name('doiMatKhau_post');
    
    //Loại phòng
    Route::group(['prefix' => 'loaiphong'], function () {
        Route::get('danhsach', 'loaiPhong_Controller@getDanhSach')->name('danhSachLoaiPhong');

        Route::get('them', 'loaiPhong_Controller@getThemLoaiPhong')->name('themLoaiPhong_get');
        Route::post('them', 'loaiPhong_Controller@postThemLoaiPhong')->name('themLoaiPhong_post');

        Route::get('sua/{id}', 'loaiPhong_Controller@getSuaLoaiPhong')->name('suaLoaiPhong_get');
        Route::post('sua/{id}', 'loaiPhong_Controller@postSuaLoaiPhong')->name('suaLoaiPhong_post');

        Route::get('xoa/{id}', 'loaiPhong_Controller@getXoaLoaiPhong')->name('xoaLoaiPhong_get');
    });

    //Phòng
    Route::group(['prefix' => 'phong'], function () {
        Route::get('danhsach', 'phong_Controller@getDanhSach')->name('danhSachPhong');

        Route::get('them', 'phong_Controller@getThemPhong')->name('themPhong_get');
        Route::post('them', 'phong_Controller@postThemPhong')->name('themPhong_post');

        Route::get('sua/{id}', 'phong_Controller@getSuaPhong')->name('suaPhong_get');
        Route::post('sua/{id}', 'phong_Controller@postSuaPhong')->name('suaPhong_post');

        Route::get('xoa/{id}', 'phong_Controller@getXoaPhong')->name('xoaPhong_get');
    });

    //Loại Bàn Tiệc
    Route::group(['prefix' => 'bantiec'], function () {
        Route::get('danhsach', 'loaiBanTiec_Controller@getDanhSach')->name('danhSachBanTiec');

        Route::get('them', 'loaiBanTiec_Controller@getThemBanTiec')->name('themBanTiec_get');
        Route::post('them', 'loaiBanTiec_Controller@postThemBanTiec')->name('themBanTiec_post');

        Route::get('sua/{id}', 'loaiBanTiec_Controller@getSuaBanTiec')->name('suaBanTiec_get');
        Route::post('sua/{id}', 'loaiBanTiec_Controller@postSuaBanTiec')->name('suaBanTiec_post');

        Route::get('xoa/{id}', 'loaiBanTiec_Controller@getXoaBanTiec')->name('xoaBanTiec_get');
    });
    //Người dùng
    Route::group(['prefix' => 'nguoidung'], function () {
        //Danh sách người dùng
        Route::get('danhsach', 'nguoiDung_Controller@getDanhSach')->name('dsNguoiDung');
        //Thêm người dùng
        Route::get('them', 'nguoiDung_Controller@getNguoiDung_Them')->name('themNguoiDung_get');
        Route::post('them', 'nguoiDung_Controller@postNguoiDung_Them')->name('themNguoiDung_post');
        //Sửa người dùng
        Route::get('sua/{id}', 'nguoiDung_Controller@getNguoiDung_Sua')->name('suaNguoiDung_get');
        Route::post('sua/{id}', 'nguoiDung_Controller@postNguoiDung_Sua')->name('suaNguoiDung_post');
        //Xóa người dùng
        Route::get('xoa/{id}', 'nguoiDung_Controller@getNguoiDung_Xoa')->name('xoaNguoiDung');
    });

    //Danh sách đặt phòng       
    Route::group(['prefix' => 'datphong'], function () {

        Route::group(['prefix' => 'danhsach'], function () {
            
            Route::get('/danhsachchoxacnhan', 'datPhong_Controller@getDanhSachChoXN')->name('dsDatPhongChoXN');
            Route::get('/danhsachchonhanphong', 'datPhong_Controller@getDanhSachChoNhanPhong')->name('dsDatPhongChoNP');
            Route::get('/danhsachcdanhanphong', 'datPhong_Controller@getDanhSachDaNhanPhong')->name('dsDatPhongDaNP');
            Route::get('/danhsachdathanhtoan', 'datPhong_Controller@getDanhSachDaThanhToan_Huy')->name('dsDatPhongDaTT_H');
        });
        Route::get('danhsach/id={id}/kiemduyet={kd}', 'datPhong_Controller@setTrangThai')->name('setTrangThai_datPhong');

    });

    //Danh sách đặt bàn       
    Route::group(['prefix' => 'datban'], function () {

        Route::group(['prefix' => 'danhsach'], function () {
            
            Route::get('/danhsachchoxacnhan', 'datBan_Controller@getDanhSachChoXN')->name('dsDatBanChoXN');
            Route::get('/danhsachchonhanban', 'datBan_Controller@getDanhSachChoNhanBan')->name('dsDatBanChoNP');
            Route::get('/danhsachdanhanban', 'datBan_Controller@getDanhSachDaNhanBan')->name('dsDatBanDaNP');
            Route::get('/danhsachdathanhtoan', 'datBan_Controller@getDanhSachDaThanhToan_Huy')->name('dsDatBanDaTT_H');
        });
        Route::get('danhsach/id={id}/kiemduyet={kd}', 'datBan_Controller@setTrangThai')->name('setTrangThai_datBan');

    });
});