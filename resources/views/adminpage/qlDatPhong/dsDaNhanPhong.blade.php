@extends('adminpage.layout.layout') @section('content')
<!-- Widgets -->
<div class="row clearfix">

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<a href="{{ route('dsDatPhongChoXN') }}">
			<div class="info-box bg-red hover-expand-effect">
				<div class="icon">
					<i class="material-icons">playlist_add_check</i>
				</div>
				<div class="content">
					<div class="text">SỐ LƯỢNG CHỜ XÁC NHẬN</div>
					<div class="number count-to" data-speed="15" data-fresh-interval="20">{{ $sl['slChoXN'] }}</div>
				</div>
			</div>
		</a>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<a href="{{ route('dsDatPhongChoNP') }}">
			<div class="info-box bg-cyan hover-expand-effect">
				<div class="icon">
					<i class="material-icons">playlist_add_check</i>
				</div>
				<div class="content">
					<div class="text">SỐ LƯỢNG CHỜ NHẬN</div>
					<div class="number count-to" data-speed="1000" data-fresh-interval="20">{{ $sl['slChoNhan'] }}</div>
				</div>
			</div>
		</a>
	</div>
</div>
<!-- Widgets -->
<div class="block-header">
	<h2 class="text-uppercase">Danh Sách Đặt Phòng</h2>
</div>
@include('adminpage.layout.success') 
@include('adminpage.layout.error') 
{{-- Danh sách chờ xác nhận --}}
<div id="tt03">
	<div class="card card-cascade narrower mt-5">
		<div class="card-body">
			<h4 class="card-title">Danh Sách Đã Nhận Phòng</h4>

			<div class="px-4">

				<!--Table-->
				<table class="table table-hover table-responsive mb-0">

					<!--Table head-->
					<thead>
						<tr class="table-info">
							<th scope="row text-center font-weight-bold" width="5%">#</th>
							<th class=" th-md  text-center font-weight-bold" width="25%">
								<a>Tên Khách

								</a>
							</th>
							<th class=" th-md text-center font-weight-bold" width="15%">
								<a>Ngày Nhận

								</a>
							</th>
							<th class=" th-md text-center font-weight-bold" width="15%">
								<a>Ngày Trả

								</a>
							</th>
							<th class=" th-md text-center font-weight-bold" width="10%">
								<a>Phòng

								</a>
							</th>
							<th class=" th-md text-center font-weight-bold" width="20%">
								<a>Duyệt

								</a>
							</th>
							<th class=" th-md text-center font-weight-bold" width="10%">
								<a href="">Chi Tiết
								</a>
							</th>
						</tr>
					</thead>
					<!--Table head-->

					<!--Table body-->
					<tbody>
						@foreach($DS_TT2 as $tt2)
						<tr>
							<th scope="row">{{ $tt2->id }}</th>
							<td>{{ $tt2->khachdat->hoTen }}</td>
							<td class="text-center">{{ $tt2->ngayNhanPhong }}</td>
							<td class="text-center">{{ $tt2->ngayTraPhong }}</td>
							<td class="text-center">{{ $tt2->phong->soPhong }}</td>
							<td class="text-center">
								<a href="{{ route('setTrangThai_datPhong', ['id' => $tt2->id, 'kd' => 3])  }}" class="btn btn-sm btn-info">Đã Nhận Phòng</a>
							</td>
							<td>
								<a class="btn btn-sm btn-success" data-toggle="modal" data-target="#chiTiet_Modal" onclick="getTT_DatPhong({{ json_encode($tt2) }});">Xem
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
					<!--Table body-->
				</table>

			</div>

			<hr class="my-0">
			<div class="d-flex justify-content-center">
				{{ $DS_TT2->links('adminpage.layout.custom_pagination') }}
			</div>
		</div>
	</div>
</div>


<!-- Central Modal Medium Info -->
<div class="modal fade" id="chiTiet_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-notify modal-info" role="document">
		<!--Content-->
		<div class="modal-content">
			<!--Header-->
			<div class="modal-header">
				<p class="heading lead">Chi Tiết Đặt Phòng</p>

				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" class="white-text">&times;</span>
				</button>
			</div>

			<!--Body-->
			<div class="modal-body">
				<table>
					<tbody>
						<tr>
							<td class="font-weight-bold">ID</td>
							<td id="id"></td>
						</tr>
						<tr>
							<td class="font-weight-bold">Tên Khách Đặt Phòng</td>
							<td id="hoTen"></td>
						</tr>
						<tr>
							<td class="font-weight-bold">CMND</td>
							<td id="cmnd"></td>
						</tr>
						<tr>
							<td class="font-weight-bold">SĐT</td>
							<td id="sdt"></td>
						</tr>
						<tr>
							<td class="font-weight-bold">Email</td>
							<td id="email"></td>
						</tr>
						<tr>
							<td class="font-weight-bold">----------------------------</td>
							<td class="font-weight-bold">----------------------------</td>
						</tr>
						<tr>
							<td class="font-weight-bold">Ngày Nhận Phòng</td>
							<td id="ngayNhanPhong"></td>
						</tr>
						<tr>
							<td class="font-weight-bold">Ngày Trả Phòng</td>
							<td id="ngayTraPhong"></td>
						</tr>
						<tr>
							<td class="font-weight-bold">Số Phòng</td>
							<td id="soPhong"></td>
						</tr>
						<tr>
							<td class="font-weight-bold">Số Người Lớn</td>
							<td id="soNguoiLon"></td>
						</tr>
						<tr>
							<td class="font-weight-bold">Số Trẻ Em</td>
							<td id="soTreEm"></td>
						</tr>
						<tr>
							<td class="font-weight-bold">Tổng Tiền</td>
							<td id="tongTien"></td>
						</tr>
					</tbody>
				</table>
			</div>

			<!--Footer-->
			<div class="modal-footer justify-content-right">
				<a type="button" class="btn btn-outline-secondary-modal waves-effect" data-dismiss="modal">Đóng</a>
			</div>
		</div>
		<!--/.Content-->
	</div>
</div>
<!-- Central Modal Medium Info-->


@endsection