@extends('adminpage.layout.layout') @section('content')
<div class="block-header">
	<h2 class="text-uppercase">Đăng Nhập</h2>
</div>
<!--Card-->
<div class="card">
	<!--Card content-->
	<div class="card-body">
		@if(session('thongbao'))
		<div class="alert alert-success">
			{{ session('thongbao') }}
		</div>
		@endif
		<form method="POST" action="{{ route('dangNhap_post') }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}" />
			<div class="md-form">
				<i class="fa fa-user prefix grey-text"></i>
				<input type="text" id="email" name="email" class="form-control">
				<label for="email">Email</label>
				@if($errors->has('email'))
				<div class="alert alert-danger">
					<strong>! {{ $errors->first('email') }}</strong>
				</div>
				@endif
			</div>
			<div class="md-form">
				<i class="fa fa-user prefix grey-text"></i>
				<input type="password" id="password" name="password" class="form-control">
				<label for="password">Mật Khẩu</label>
				@if($errors->has('password'))
				<div class="alert alert-danger">
					<strong>! {{ $errors->first('password') }}</strong>
				</div>
				@endif
			</div>
			<div class="text-center">
				<button class="btn btn-unique">Đăng Nhập
					<i class="far fa-share-square"></i>
				</button>
			</div>
		</form>
	</div>

</div>
<!--/.Card-->
@endsection