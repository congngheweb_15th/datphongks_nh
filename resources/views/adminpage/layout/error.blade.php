@if(session('error'))
<div class="card-content">
	<div class="alert alert-danger">
		{{ session('error') }}
	</div>
</div>
@endif