@if ($paginator->hasPages())
<!--Pagination blue-->
<nav class="my-4">
    <ul class="pagination pagination-circle pg-blue mb-0">
        <!--Arrow left-->
         {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
        <!--First-->
        <li class="page-item disabled"><a class="page-link">Đầu tiên</a></li>
        @else
        <li class="page-item">
            <a class="page-link" aria-label="Previous" a href="{{ $paginator->previousPageUrl() }}" rel="prev">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">Trước</span></span>
            </a>
        </li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="disabled"><span>{{ $element }}</span></li>
            @endif
            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                    <!--Numbers-->
                    <li class="page-item active"><a class="page-link">{{ $page }}</a></li>
                    @else
                    <li class="page-item" ><a class="page-link" href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
        <!--Arrow right-->
        <li class="page-item">
            <a class="page-link" aria-label="Next" href="{{ $paginator->nextPageUrl() }}" rel="next">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Tiếp</span>
            </a>
        </li>
        @else
        <li class="page-item disabled"><a class="page-link">Cuối Cùng</a></li>
        @endif 
    </ul>
</nav>
<!--/Pagination blue-->
@endif