@if(session('success'))
<div class="card-content">
	<div class="alert alert-success">
		{{ session('success') }}
	</div>
</div>
@endif