<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>iLuzence | Hệ Thống Khách Sạn & Nhà Hàng Nổi Tiếng Nhất Việt Nam</title>

	<!-- CSS -->
	<link rel="stylesheet" href="{{ asset('public/css/bootstrap.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('public/css/mdb.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('public/css/waves.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('public/css/theme-orange.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('public/css/materialize.css') }}" />
	<link rel="stylesheet" href="{{ asset('public/css/admin_style.min.css') }}" />
	<link rel="stylesheet" href="{{ asset('public/css/custom.css') }}" /> {{-- Font Icon --}}
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="{{ asset('public/css/fa-svg-with-js.css') }}" />

<!-- favicon -->
	<link rel="shortcut icon" href="{{ asset('public/img/favicon.ico') }}" type="image/x-icon">
	<link rel="icon" href="{{ asset('public/img/favicon.ico') }}" type="image/x-icon">
</head>

<body class="theme-orange">
	<!-- Top Bar -->
	<nav class="navbar">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="{{ url('/') }}" class="text-uppercase">Quản Lý Khách Sạn & Nhà Hàng iLuzence</a>
			</div>
			<ul class="navbar-nav ml-auto ">
				@if(!empty(Auth::user()))
				<li class="nav-item">
					<div class="user-info">
						<img src="{{ asset('public/img/user.png') }}" width="38" height="38" style="border-radius: 30px;" alt="User" />
						<span class="text-white">Chào, {{ Auth::user()->name }}</span>
					</div>
				</li>
				@else
				<li class="nav-item">
					<div class="user-info">
						<a href="{{ route('dangNhap_get') }}" class="btn btn-success">Đăng Nhập</a>
					</div>
				</li>
				@endif
			</ul>
		</div>
	</nav>
	<!-- #Top Bar -->
	<section>
		<!-- Left Sidebar -->
		<aside id="leftsidebar" class="sidebar">
			<!-- Menu -->
			<div class="menu">
				<ul class="list">
					<li class="header">Danh Mục</li>
					@if(!empty(Auth::user())) 
						@if((Auth::user()->phanQuyen == 1))
							<li>
								<a href="{{ route('adminpage') }}">
									<i class="material-icons">home</i>
									<span>Trang Chủ</span>
								</a>
							</li>
							<li>
								<a class="menu-toggle">
									<i class="material-icons">assignment</i>
									<span>Quản Lý Danh Sách Đặt Phòng</span>
								</a>
								<ul class="ml-menu">
									<li>
										<a href="{{ route('dsDatPhongChoXN') }}">Danh Sách Chờ Xác Nhận</a>
									</li>
									<li>
										<a href="{{ route('dsDatPhongChoNP') }}">Danh Sách Chờ Nhận Phòng</a>
									</li>
									<li>
										<a href="{{ route('dsDatPhongDaNP') }}">Danh Sách Đã Nhận Phòng</a>
									</li>
									<li>
										<a href="{{ route('dsDatPhongDaTT_H') }}">Danh Sách Đã Thanh Toán & Hủy</a>
									</li>
								</ul>
							</li>
							<li>
								<a class="menu-toggle">
									<i class="material-icons">assignment</i>
									<span>Quản Lý Danh Sách Đặt Bàn</span>
								</a>
								<ul class="ml-menu">
									<li>
										<a href="{{ route('dsDatBanChoXN') }}">Danh Sách Chờ Xác Nhận</a>
									</li>
									<li>
										<a href="{{ route('dsDatBanChoNP') }}">Danh Sách Chờ Nhận Bàn</a>
									</li>
									<li>
										<a href="{{ route('dsDatBanDaNP') }}">Danh Sách Đã Nhận Bàn</a>
									</li>
									<li>
										<a href="{{ route('dsDatBanDaTT_H') }}">Danh Sách Đã Thanh Toán & Hủy</a>
									</li>
								</ul>
							</li>
							<li>
								<a href="javascript:void(0);" class="menu-toggle">
									<i class="material-icons">assignment</i>
									<span>Quản Lý Phòng</span>
								</a>
								<ul class="ml-menu">
									<li>
										<a href="{{ route('danhSachLoaiPhong') }}">Quản Lý Loại Phòng</a>
									</li>
									<li>
										<a href="{{ route('danhSachPhong') }}">Quản Lý Phòng</a>
									</li>
								</ul>
							</li>
							<li>
								<a href="{{ route('danhSachBanTiec') }}">
									<i class="material-icons">assignment</i>
									<span>Quản Lý Bàn Tiệc Nhà Hàng</span>
								</a>
							</li>
							<li>
								<a href="{{ route('dsNguoiDung') }}">
									<i class="material-icons">assignment</i>
									<span>Quản Lý Người Dùng</span>
								</a>
							</li>
						@else
							<li>
								<a href="{{ route('adminpage') }}">
									<i class="material-icons">home</i>
									<span>Trang Chủ</span>
								</a>
							</li>
							<li>
								<a class="menu-toggle">
									<i class="material-icons">assignment</i>
									<span>Quản Lý Danh Sách Đặt Phòng</span>
								</a>
								<ul class="ml-menu">
									<li>
										<a href="{{ route('dsDatPhongChoXN') }}">Danh Sách Chờ Xác Nhận</a>
									</li>
									<li>
										<a href="{{ route('dsDatPhongChoNP') }}">Danh Sách Chờ Nhận Phòng</a>
									</li>
									<li>
										<a href="{{ route('dsDatPhongDaNP') }}">Danh Sách Đã Nhận Phòng</a>
									</li>
									<li>
										<a href="{{ route('dsDatPhongDaTT_H') }}">Danh Sách Đã Thanh Toán & Hủy</a>
									</li>
								</ul>
							</li>
							<li>
								<a class="menu-toggle">
									<i class="material-icons">assignment</i>
									<span>Quản Lý Danh Sách Đặt Bàn</span>
								</a>
								<ul class="ml-menu">
									<li>
										<a href="{{ route('dsDatBanChoXN') }}">Danh Sách Chờ Xác Nhận</a>
									</li>
									<li>
										<a href="{{ route('dsDatBanChoNP') }}">Danh Sách Chờ Nhận Bàn</a>
									</li>
									<li>
										<a href="{{ route('dsDatBanDaNP') }}">Danh Sách Đã Nhận Bàn</a>
									</li>
									<li>
										<a href="{{ route('dsDatBanDaTT_H') }}">Danh Sách Đã Thanh Toán & Hủy</a>
									</li>
								</ul>
							</li>

					@endif
				</ul>
				@endif
			</div>
			<!-- #Menu -->
			<!-- Footer -->
			<div class="legal">
				<div class="copyright d-flex justify-content-center">
					@if(!empty(Auth::user()))
					<div>
						<a href="{{ route('doiMatKhau_get', ['id' => Auth::user()->id]) }}" class="btn btn-sm btn-outline-info" style="font-size: 11px;">Đổi Mật Khẩu</a>
					</div>
					<div>
						<a href="{{ route('dangXuat') }}" class="btn btn-sm btn-outline-danger" style="font-size: 11px;">Đăng Xuất</a>
					</div>
				</div>
				@endif
				<div class="version">
					&copy; 2016 - 2017 | DTH146751
				</div>
			</div>
			<!-- #Footer -->
		</aside>
		<!-- #END Left Sidebar -->
	</section>

	{{-- #Content --}}
	<section class="content">
		<div class="container-fluid">
			@yield('content')
		</div>
	</section>
	{{-- #End Content --}}

	<!-- Scripts -->
	<script src="{{ asset('public/js/jquery-3.2.1.min.js') }}"></script>
	<script src="{{ asset('public/js/popper.min.js') }}"></script>
	<script src="{{ asset('public/js/bootstrap.min.js') }}"></script>
	<script src="{{ asset('public/js/mdb.min.js') }}"></script>
	<script src="{{ asset('public/js/waves.min.js') }}"></script>
	<script src="{{ asset('public/js/admin.js') }}"></script>
	<script src="{{ asset('public/js/fontawesome-all.min.js') }}"></script>
	<script src="{{ asset('public/js/chiTietModal_script.js') }}"></script>

</body>

</html>