@extends('adminpage.layout.layout')

@section('content')

<div class="block-header">
	<h2 class="text-uppercase">Danh Sách Phòng</h2>
</div>
<div class="card card-cascade narrower mt-5">
	<div class="card-body">
		@include('adminpage.layout.success')
		@include('adminpage.layout.error')
		<a class="btn btn-sm btn-success" href="{{ route('themPhong_get') }}">
			<i class="material-icons">add</i>
			</i> Thêm </a>
		<div class="px-4">

			<!--Table-->
			<table class="table table-hover table-responsive mb-0">

				<!--Table head-->
				<thead>
					<tr class="table-info">
						<th scope="row font-weight-bold" width="5%">#</th>
						<th class="th-md font-weight-bold  text-center" width="45%">
							<a>Số Phòng
								<i class="fa fa-sort ml-1"></i>
							</a>
						</th>
						<th class="th-md font-weight-bold text-center"  width="40%">
							<a>Loại Phòng
								<i class="fa fa-sort ml-1"></i>
							</a>
						</th>
						<th class="th-md text-center font-weight-bold" width="5%">
							<a href="">Sửa

							</a>
						</th>
						<th class="th-md text-center font-weight-bold" width="5%">
							<a href="">Xóa

							</a>
						</th>
					</tr>
				</thead>
				<!--Table head-->

				<!--Table body-->
				<tbody>
					@foreach($phong as $p)
					<tr>
						<th scope="row" class="text-center">{{ $p->id }}</th>
						<td class="text-center">{{ $p->soPhong }}</td>
						<td class="text-center">{{ $p->loaiphong->tenLoaiPhong }}</td>
						<td class="text-center">
							<a href="{{ route('suaPhong_get', ['id' => $p->id]) }}">
								<img src="{{ asset('public/img/edit.svg') }}" width="24px" />
							</a>
						</td>
						<td class="text-center">
							<a href="{{ route('xoaPhong_get', ['id' => $p->id]) }}">
								<img src="{{ asset('public/img/delete.svg') }}" width="24px" />
							</a>
						</td>
					</tr>
					@endforeach
				</tbody>
				<!--Table body-->
			</table>
			<div class="d-flex justify-content-center">
				{{ $phong->links('adminpage.layout.custom_pagination') }}
			</div>
		</div>
	</div>
</div>

@endsection