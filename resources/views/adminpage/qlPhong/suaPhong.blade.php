@extends('adminpage.layout.layout') @section('content') {{-- CKFinder --}}

<div class="block-header">
	<h2 class="text-uppercase">Sửa Thông Tin Phòng Mới</h2>
</div>
<!--Card-->
<div class="card">
	@include('adminpage.layout.success')
	<!--Card content-->
	<div class="card-body">
		<form action="{{ route('suaPhong_post', ['id' => $phong->id]) }}" method="POST">
			{{ csrf_field() }}
			<div class="md-form">
				<i class="fa fa-user prefix grey-text"></i>
				<input type="text" id="soPhong" name="soPhong" class="form-control" value="{{ $phong->soPhong }}">
				<label for="soPhong">Số Phòng</label>
				@if($errors->has('soPhong'))
				<div class="alert alert-danger">
					<strong>! {{ $errors->first('soPhong') }}</strong>
				</div>
				@endif
			</div>
			<div class="form-group">
				<i class="fa fa-user prefix grey-text"></i>
				<label for="loaiPhong">Loại Phòng</label>
				<select class="form-control" name="loaiPhong" id="loaiPhong">
					<option value="" disabled selected>---Chọn Loại Phòng---</option>
                    @foreach($loaiPhong as $lp)
                        <option value="{{ $lp->id }}" @if($lp->id == $phong->loaiphong->id) selected @endif>{{ $lp->tenLoaiPhong }}</option>
                    @endforeach
				</select>
				@if($errors->has('loaiPhong'))
				<div class="alert alert-danger">
					<strong>! {{ $errors->first('loaiPhong') }}</strong>
				</div>
				@endif
			</div>
			<div class="text-center">
				<button class="btn btn-unique">Cập Nhật Vào CSDL
					<i class="fa fa-paper-plane-o ml-1"></i>
				</button>
			</div>
		</form>
	</div>
</div>
<!--/.Card-->
@endsection