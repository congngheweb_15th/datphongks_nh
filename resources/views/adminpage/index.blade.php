@extends('adminpage.layout.layout')
@section('content')

<div class="block-header">
	<h2 class="text-uppercase">Danh Sách Chờ Xác Nhận Đặt Phòng</h2>
</div>

<!-- Widgets -->
<div class="row clearfix">

	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<a href="{{ route('dsDatPhongChoXN') }}">
			<div class="info-box bg-red hover-expand-effect">
				<div class="icon">
					<i class="material-icons">playlist_add_check</i>
				</div>
				<div class="content">
					<div class="text">SỐ LƯỢNG CHỜ XÁC NHẬN</div>
					<div class="number count-to" data-speed="15" data-fresh-interval="20">{{ $sl['slChoXN'] }}</div>
				</div>
			</div>
		</a>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<a href="{{ route('dsDatPhongChoNP') }}">
			<div class="info-box bg-cyan hover-expand-effect">
				<div class="icon">
					<i class="material-icons">playlist_add_check</i>
				</div>
				<div class="content">
					<div class="text">SỐ LƯỢNG CHỜ NHẬN</div>
					<div class="number count-to" data-speed="1000" data-fresh-interval="20">{{ $sl['slChoNhan'] }}</div>
				</div>
			</div>
		</a>
	</div>
</div>
<!-- Widgets -->

<div class="block-header">
	<h2 class="text-uppercase">Danh Sách Chờ Xác Nhận Đặt Bàn</h2>
</div>

<!-- Widgets -->
<div class="row clearfix">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<a href="{{ route('dsDatBanChoXN')}}">
			<div class="info-box bg-red hover-expand-effect">
				<div class="icon">
					<i class="material-icons">playlist_add_check</i>
				</div>
				<div class="content">
					<div class="text">SỐ LƯỢNG CHỜ XÁC NHẬN</div>
					<div class="number count-to" data-speed="15" data-fresh-interval="20">{{ $sl2['slChoXN'] }}</div>
				</div>
			</div>

	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<a href="{{ route('dsDatBanChoNP')}}">
			<div class="info-box bg-cyan hover-expand-effect">
				<div class="icon">
					<i class="material-icons">playlist_add_check</i>
				</div>
				<div class="content">
					<div class="text">SỐ LƯỢNG CHỜ NHẬN</div>
					<div class="number count-to" data-speed="1000" data-fresh-interval="20">{{ $sl2['slChoNhan'] }}</div>
				</div>
			</div>
		</a>
	</div>
</div>
<!-- Widgets -->


@endsection