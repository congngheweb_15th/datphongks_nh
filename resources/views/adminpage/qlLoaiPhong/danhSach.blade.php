@extends('adminpage.layout.layout') @section('content')


<div class="block-header">
	<h2 class="text-uppercase">Danh Sách Các Loại Phòng</h2>
</div>
<div class="card card-cascade narrower mt-5">
	<div class="card-body">
		@include('adminpage.layout.success') @include('adminpage.layout.error')
		<a class="btn btn-sm btn-success" href="{{ route('themLoaiPhong_get') }}">
			<i class="material-icons">add</i>
			</i> Thêm </a>
		<div class="px-4">

			<!--Table-->
			<table class="table table-hover table-responsive mb-0">

				<!--Table head-->
				<thead>
					<tr class="table-info">
						<th scope="row font-weight-bold" width="5%">#</th>
						<th class="th-md font-weight-bold  text-center" width="25%">
							<a>Tên Loại Phòng
								<i class="fa fa-sort ml-1"></i>
							</a>
						</th>
						<th class="th-md font-weight-bold text-center" width="10%">
							<a>Giá Phòng
								<i class="fa fa-sort ml-1"></i>
							</a>
						</th>
						<th class="th-md font-weight-bold  text-center" width="20%">
							<a>Ảnh Minh Họa
							</a>
						</th>
						<th class="th-md font-weight-bold  text-center" width="30%">
							<a>Mô Tả
							</a>
						</th>

						<th class="th-md text-center font-weight-bold" width="5%">
							<a href="">Sửa

							</a>
						</th>
						<th class="th-md text-center font-weight-bold" width="5%">
							<a href="">Xóa

							</a>
						</th>
					</tr>
				</thead>
				<!--Table head-->

				<!--Table body-->
				<tbody>
					@foreach($loaiPhong as $lp)
					<tr>
						<th scope="row" class="text-center">{{ $lp->id }}</th>
						<td class="text-center">{{ $lp->tenLoaiPhong }}</td>
						<td class="text-right">{{ $lp->giaLoaiPhong }} VND</td>
						<td class="text-center">
							<img src="../../public/uploads/images/{{ $lp->hinhAnhMinhHoa }}" width="100px" />
						</td>
						<td>{{ $lp->moTa }}</td>
						<td class="text-center">
							<a href="{{ route('suaLoaiPhong_get', ['id' => $lp->id]) }}">
								<img src="{{ asset('public/img/edit.svg') }}" width="24px" />
							</a>
						</td>
						<td class="text-center">
							<a href="{{ route('xoaLoaiPhong_get', ['id' => $lp->id]) }}">
								<img src="{{ asset('public/img/delete.svg') }}" width="24px" />
							</a>
						</td>
					</tr>
					@endforeach
				</tbody>
				<!--Table body-->
			</table>
			<div class="d-flex justify-content-center">
				{{ $loaiPhong->links('adminpage.layout.custom_pagination') }}
			</div>
		</div>
	</div>
</div>

@endsection