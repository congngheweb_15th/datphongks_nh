@extends('adminpage.layout.layout') @section('content') {{-- CKFinder --}}
<script src="{{ asset('public/js/ckfinder/ckfinder.js') }}"></script>

<div class="block-header">
	<h2 class="text-uppercase">Thêm Loại Phòng Mới</h2>
</div>
<!--Card-->
<div class="card">
	@include('adminpage.layout.success')
	<!--Card content-->
	<div class="card-body">
		<form action="{{ route('themLoaiPhong_post') }}" method="POST">
			{{ csrf_field() }}
			<div class="md-form">
				<i class="fa fa-user prefix grey-text"></i>
				<input type="text" id="tenLoaiPhong" name="tenLoaiPhong" class="form-control">
				<label for="tenLoaiPhong">Tên Loại Phòng</label>
				@if($errors->has('tenLoaiPhong'))
				<div class="alert alert-danger">
					<strong>! {{ $errors->first('tenLoaiPhong') }}</strong>
				</div>
				@endif
			</div>
			<div class="md-form">
				<i class="fa fa-user prefix grey-text"></i>
				<input type="number" id="giaLoaiPhong" name="giaLoaiPhong" class="form-control">
				<label for="giaLoaiPhong">Đơn Giá Loại Phòng</label>
				@if($errors->has('giaLoaiPhong'))
				<div class="alert alert-danger">
					<strong>! {{ $errors->first('giaLoaiPhong') }}</strong>
				</div>
				@endif
			</div>
			<div class="md-form">
				<div class="row">
					<div class="col-sm-1">
						<a id="chonAnh" name="chonAnh" onclick="openPopup()">
							<img src="{{ asset('public/img/upload.svg') }}" width="50px" />
						</a>
					</div>
					<div class="col-sm-11">
						<input type="text" id="hinhAnh" name="hinhAnh" class="form-control" placeholder="Chọn hình ảnh minh họa" Readonly>
					</div>
				</div>
				@if($errors->has('hinhAnh'))
				<div class="alert alert-danger">
					<strong>! {{ $errors->first('hinhAnh') }}</strong>
				</div>
				@endif
			</div>
			<!--Material textarea-->
			<div class="md-form">
				<i class="fa fa-user prefix grey-text"></i>
				<textarea type="text" class="md-textarea" name="moTa" id="moTa" cols="30" rows="10"></textarea>
				<label for="moTa">Mô Tả</label>
				@if($errors->has('moTa'))
				<div class="alert alert-danger">
					<strong>! {{ $errors->first('moTa') }}</strong>
				</div>
				@endif
			</div>
			<div class="text-center">
				<button class="btn btn-unique">Thêm Vào CSDL
					<i class="fa fa-paper-plane-o ml-1"></i>
				</button>
			</div>
		</form>
	</div>
</div>
<!--/.Card-->

<script>
	function openPopup() {
		CKFinder.popup( {
			chooseFiles: true,
			onInit: function( finder ) {
				finder.on( 'files:choose', function( evt ) {
					var file = evt.data.files.first();
					document.getElementById( 'hinhAnh' ).value = file.getUrl().substring(file.getUrl().lastIndexOf('/') + 1);
				} );
			}
		});				
	}

</script>
@endsection