@extends('adminpage.layout.layout') @section('content')
<!-- Widgets -->
<div class="row clearfix">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<a href="{{ route('dsDatBanChoXN')}}">
			<div class="info-box bg-red hover-expand-effect">
				<div class="icon">
					<i class="material-icons">playlist_add_check</i>
				</div>
				<div class="content">
					<div class="text">SỐ LƯỢNG CHỜ XÁC NHẬN</div>
					<div class="number count-to" data-speed="15" data-fresh-interval="20">{{ $sl['slChoXN'] }}</div>
				</div>
			</div>

	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<a href="{{ route('dsDatBanChoNP')}}">
			<div class="info-box bg-cyan hover-expand-effect">
				<div class="icon">
					<i class="material-icons">playlist_add_check</i>
				</div>
				<div class="content">
					<div class="text">SỐ LƯỢNG CHỜ NHẬN</div>
					<div class="number count-to" data-speed="1000" data-fresh-interval="20">{{ $sl['slChoNhan'] }}</div>
				</div>
			</div>
		</a>
	</div>
</div>
<!-- Widgets -->
<div class="block-header">
	<h2 class="text-uppercase">Danh Sách Đặt Bàn Nhà Hàng</h2>
</div>
@include('adminpage.layout.success') @include('adminpage.layout.error') {{-- Danh sách chờ xác nhận --}}
<div id="tt01">
	<div class="card card-cascade narrower mt-5">
		<div class="card-body">
			<h4 class="card-title">Danh Sách Chờ Nhận Bàn</h4>

			<div class="px-4">

				<!--Table-->
				<table class="table table-hover table-responsive mb-0">

					<!--Table head-->
					<thead>
						<tr class="table-info">
							<th scope="row text-center font-weight-bold" width="5%">#</th>
							<th class=" th-md  text-center font-weight-bold" width="15%">
								<a>Tên Khách

								</a>
							</th>
							<th class=" th-md text-center font-weight-bold" width="20%">
								<a>Thời Gian Nhận Bàn

								</a>
							</th>
							<th class=" th-md text-center font-weight-bold" width="10%">
								<a>Loại Bàn

								</a>
							</th>
							<th class=" th-md text-center font-weight-bold" width="15%">
								<a>Ghi Chú

								</a>
							</th>
							<th class=" th-md text-center font-weight-bold" width="20%">
								<a>Duyệt

								</a>
							</th>
							<th class=" th-md text-center font-weight-bold" width="10%">
								<a href="">Chi Tiết
								</a>
							</th>
							<th class=" th-md text-center font-weight-bold" width="5%">
								<a href="">Hủy

								</a>
							</th>
						</tr>
					</thead>
					<!--Table head-->

					<!--Table body-->
					<tbody>
						@foreach($DS_TT1 as $tt1)
						<tr>
							<th scope="row">{{ $tt1->id }}</th>
							<td>{{ $tt1->khachdat->hoTen }}</td>
							<td class="text-center">
								{{ $tt1->gioNhan }} | {{ $tt1->ngayNhan }}
							</td>
							<td class="text-center">{{ $tt1->loaiban->tenLoaiBan }}</td>

							<td class="text-center">{{ $tt1->ghiChu }}</td>
							<td class="text-center">
								<a href="{{ route('setTrangThai_datBan', ['id' => $tt1->id, 'kd' => 2]) }}" class="btn btn-sm btn-info">Đã Nhận Bàn
									</>
							</td>
							<td class="text-center">
								<a class="btn btn-sm btn-success" data-toggle="modal" data-target="#chiTiet_Modal" onclick="getTT_DatBan({{ json_encode($tt1) }});">Xem
								</a>
							</td>
							<td class="text-center">
								<a href="{{ route('setTrangThai_datBan', ['id' => $tt1->id, 'kd' => 4])  }}" class="btn btn-sm btn-danger">
									Hủy
								</a>
							</td>
						</tr>
						@endforeach
					</tbody>
					<!--Table body-->
				</table>

			</div>

			<hr class="my-0">
			<div class="d-flex justify-content-center">
				{{ $DS_TT1->links('adminpage.layout.custom_pagination') }}
			</div>
		</div>
	</div>
</div>

<!-- Central Modal Medium Info -->
<div class="modal fade" id="chiTiet_Modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-notify modal-info" role="document">
		<!--Content-->
		<div class="modal-content">
			<!--Header-->
			<div class="modal-header">
				<p class="heading lead">Chi Tiết Đặt Bàn</p>

				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true" class="white-text">&times;</span>
				</button>
			</div>

			<!--Body-->
			<div class="modal-body">
				<table>
					<tbody>
						<tr>
							<td class="font-weight-bold">ID</td>
							<td id="id"></td>
						</tr>
						<tr>
							<td class="font-weight-bold">Tên Khách Đặt Phòng</td>
							<td id="hoTen"></td>
						</tr>
						<tr>
							<td class="font-weight-bold">CMND</td>
							<td id="cmnd"></td>
						</tr>
						<tr>
							<td class="font-weight-bold">SĐT</td>
							<td id="sdt"></td>
						</tr>
						<tr>
							<td class="font-weight-bold">Email</td>
							<td id="email"></td>
						</tr>
						<tr>
							<td class="font-weight-bold">----------------------------</td>
							<td class="font-weight-bold">----------------------------</td>
						</tr>
						<tr>
							<td class="font-weight-bold">Thời Gian Đến Nhận Bàn</td>
							<td id="thoiGianNhan"></td>
						</tr>
						<tr>
							<td class="font-weight-bold">Số Lượng Khách</td>
							<td id="slKhach"></td>
						</tr>
						<tr>
							<td class="font-weight-bold">Số Lượng Bàn</td>
							<td id="slBan"></td>
						</tr>
						<tr>
							<td class="font-weight-bold">Địa Điểm Đặt Bàn</td>
							<td id="diaDiem"></td>
						</tr>
						<tr>
							<td class="font-weight-bold">Ghi Chú</td>
							<td id="ghiChu"></td>
						</tr>
						<tr>
							<td class="font-weight-bold">Tổng Tiền</td>
							<td id="tongTien"></td>
						</tr>
					</tbody>
				</table>
			</div>

			<!--Footer-->
			<div class="modal-footer justify-content-right">
				<a type="button" class="btn btn-outline-secondary-modal waves-effect" data-dismiss="modal">Đóng</a>
			</div>
		</div>
		<!--/.Content-->
	</div>
</div>
<!-- Central Modal Medium Info-->

@endsection