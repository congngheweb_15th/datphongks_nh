@extends('adminpage.layout.layout') @section('content')

<div class="block-header">
	<h2 class="text-uppercase">Danh Sách Tài Khoản Người Dùng</h2>
</div>
<div class="card card-cascade narrower mt-5">
	<div class="card-body">
		@if(session('success'))
		<div class="alert alert-success">
			{{ session('success') }}
		</div>
		@endif
		<a class="btn btn-sm btn-success" href="{{ route('themNguoiDung_get') }}">
			<i class="material-icons">add</i>
			</i> Thêm </a>
		<div class="px-4">

			<!--Table-->
			<table class="table table-hover table-responsive mb-0">

				<!--Table head-->
				<thead>
					<tr class="table-info">
						<th scope="row font-weight-bold" width="5%">#</th>
						<th class="th-md font-weight-bold" width="40%">
							<a>Tên Tài Khoản
								<i class="fa fa-sort ml-1"></i>
							</a>
						</th>
						<th class="th-md font-weight-bold" width="30%">
							<a>Email
								<i class="fa fa-sort ml-1"></i>
							</a>
						</th>
						<th class="th-md font-weight-bold" width="15%">
							<a>Quyền Hạn
								<i class="fa fa-sort ml-1"></i>
							</a>
						</th>
						<th class="th-md text-center font-weight-bold" width="5%">
							<a href="">Sửa

							</a>
						</th>
						<th class="th-md text-center font-weight-bold" width="5%">
							<a href="">Xóa

							</a>
						</th>
					</tr>
				</thead>
				<!--Table head-->

				<!--Table body-->
				<tbody>
					@foreach($user as $u)
					<tr>
						<th scope="row">{{ $u->id }}</th>
						<td>{{ $u->name }}</td>
						<td>{{ $u->email }}</td>
						<td class="text-center">
							@if($u->phanQuyen == 1)
								<span class="btn btn-sm btn-info">Administrator</span>
							@else
								<span class="btn btn-sm btn-info">User</span>
							@endif
						</td>
						<td class="text-center">
							<a href="{{ route('suaNguoiDung_get', ['id'=>$u->id]) }}">
								<img src="{{ asset('public/img/edit.svg') }}" width="24px" />
							</a>
						</td>
						<td class="text-center">
							<a href="{{ route('xoaNguoiDung', ['id'=>$u->id]) }}">
								<img src="{{ asset('public/img/delete.svg') }}" width="24px" />
							</a>
						</td>
					</tr>
					@endforeach
				</tbody>
				<!--Table body-->
			</table>

		</div>

		<hr class="my-0">

		<!--Bottom Table UI-->
		<div class="d-flex justify-content-center">
			{{ $user->links('adminpage.layout.custom_pagination') }}
		</div>
		<!--Bottom Table UI-->
	</div>
</div>


@endsection