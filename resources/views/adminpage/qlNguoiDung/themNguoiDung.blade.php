@extends('adminpage.layout.layout') 
@section('content')
<div class="block-header">
	<h2 class="text-uppercase">Thêm Người Dùng Mới</h2>
</div>
<!--Card-->
<div class="card">

	<!--Card content-->
	<div class="card-body">
		@if(session('success'))
		<div class="alert alert-success">
			{{ session('success') }}
		</div>
		@endif
		<form action="{{ route('themNguoiDung_post') }}" method="POST">
			{{ csrf_field() }}
			<div class="md-form">
				<i class="fa fa-user prefix grey-text"></i>
				<input type="text" id="name" name="name" class="form-control">
				<label for="name">Tên Tài Khoản</label>
				@if($errors->has('name'))
				<div class="alert alert-danger">
					<strong>! {{ $errors->first('name') }}</strong>
				</div>
				@endif
			</div>
            <div class="md-form">
				<i class="fa fa-user prefix grey-text"></i>
				<input type="text" id="email" name="email" class="form-control">
				<label for="email">Email</label>
				@if($errors->has('email'))
				<div class="alert alert-danger">
					<strong>! {{ $errors->first('email') }}</strong>
				</div>
				@endif
			</div>
            <div class="md-form">
				<i class="fa fa-user prefix grey-text"></i>
				<input type="password" id="password" name="password" class="form-control">
				<label for="password">Mật Khẩu</label>
				@if($errors->has('password'))
				<div class="alert alert-danger">
					<strong>! {{ $errors->first('password') }}</strong>
				</div>
				@endif
			</div>
             <div class="md-form">
				<i class="fa fa-user prefix grey-text"></i>
				<input type="password" id="password-confirm" name="password-confirm" class="form-control">
				<label for="password-confirm">Xác Nhận Mật Khẩu</label>
			</div>
            <div class="form-group">
				<i class="fa fa-user prefix grey-text"></i>
				<label for="phanquyen">Quyền Hạn</label>
				<select class="form-control" name="phanquyen" id="phanquyen">
					<option value="" disabled selected>---Chọn Quyền Hạn---</option>
					<option value="1">Administrator</option>
                    <option value="2">User</option>
				</select>
				@if($errors->has('phanquyen'))
				<div class="alert alert-danger">
					<strong>! {{ $errors->first('phanquyen') }}</strong>
				</div>
				@endif
			</div>
			<div class="text-center">
				<button class="btn btn-unique">Thêm Vào CSDL
					<i class="fa fa-paper-plane-o ml-1"></i>
				</button>
			</div>
		</form>
	</div>

</div>
<!--/.Card-->
@endsection