@extends('adminpage.layout.layout') @section('content')
<div class="block-header">
	<h2 class="text-uppercase">Chỉnh Sửa Tài Khoản</h2>
</div>
<!--Card-->
<div class="card">

	<!--Card content-->
	<div class="card-body">
		@if(session('success'))
		<div class="alert alert-success">
			{{ session('success') }}
		</div>
		@endif
		<form action="{{ route('suaNguoiDung_post', ['id' => $nguoiDung->id] ) }}" method="POST">
			{{ csrf_field() }}
			<div class="md-form">
				<i class="fa fa-user prefix grey-text"></i>
				<input type="text" id="name" name="name" class="form-control" value="{{ $nguoiDung->name }}">
				<label for="name">Tên Tài Khoản</label>
				@if($errors->has('name'))
				<div class="alert alert-danger">
					<strong>! {{ $errors->first('name') }}</strong>
				</div>
				@endif
			</div>
            <div class="form-group">
				<i class="fa fa-user prefix grey-text"></i>
				<label for="phanquyen">Quyền Hạn</label>
				<select class="form-control" name="phanquyen" id="phanquyen">
					<option value="" disabled selected>---Chọn Quyền Hạn---</option>
                    @if($nguoiDung->phanquyen == 1)
					    <option value="1" selected>Administrator</option>
                        <option value="2">User</option>
                    @else
                        <option value="1" >Administrator</option>
                        <option value="2" selected>User</option>
                    @endif
				</select>
				@if($errors->has('phanquyen'))
				<div class="alert alert-danger">
					<strong>! {{ $errors->first('phanquyen') }}</strong>
				</div>
				@endif
			</div>
			<div class="text-center">
				<button class="btn btn-unique">Cập Nhật Vào CSDL
					<i class="fa fa-paper-plane-o ml-1"></i>
				</button>
			</div>
		</form>
	</div>

</div>
<!--/.Card-->
@endsection