@extends('adminpage.layout.layout') @section('content') {{-- CKFinder --}}
<script src="{{ asset('public/js/ckfinder/ckfinder.js') }}"></script>

<div class="block-header">
	<h2 class="text-uppercase">Sửa Thông Tin Loại Bàn</h2>
</div>
<!--Card-->
<div class="card">
	@include('adminpage.layout.success')
	<!--Card content-->
	<div class="card-body">
		<form action="{{ route('suaBanTiec_post', ['id' => $loaiBan->id]) }}" method="POST">
			{{ csrf_field() }}
			<div class="md-form">
				<i class="fa fa-user prefix grey-text"></i>
				<input type="text" id="tenLoaiBan" name="tenLoaiBan" class="form-control" value="{{ $loaiBan->tenLoaiBan }}">
				<label for="tenLoaiBan">Tên Loại Bàn</label>
				@if($errors->has('tenLoaiBan'))
				<div class="alert alert-danger">
					<strong>! {{ $errors->first('tenLoaiBan') }}</strong>
				</div>
				@endif
			</div>
			<div class="md-form">
				<i class="fa fa-user prefix grey-text"></i>
				<input type="number" id="donGiaLoaiBan" name="donGiaLoaiBan" class="form-control" value="{{ $loaiBan->donGiaLoaiBan }}">
				<label for="donGiaLoaiBan">Đơn Giá Loại Phòng</label>
				@if($errors->has('donGiaLoaiBan'))
				<div class="alert alert-danger">
					<strong>! {{ $errors->first('donGiaLoaiBan') }}</strong>
				</div>
				@endif
			</div>
			<div class="md-form">
				<div class="row">
					<div class="col-sm-1">
						<a id="chonAnh" name="chonAnh" onclick="openPopup()">
							<img src="{{ asset('public/img/upload.svg') }}" width="50px" />
						</a>
					</div>
					<div class="col-sm-11">
						<input type="text" id="hinhAnh" name="hinhAnh" class="form-control" placeholder="Chọn hình ảnh minh họa" Readonly value="{{ $loaiBan->hinhAnhMinhHoa }}">
					</div>
				</div>
				@if($errors->has('hinhAnh'))
				<div class="alert alert-danger">
					<strong>! {{ $errors->first('hinhAnh') }}</strong>
				</div>
				@endif
			</div>
			<!--Material textarea-->
			<div class="md-form">
				<i class="fa fa-user prefix grey-text"></i>
				<textarea type="text" class="md-textarea" name="moTa" id="moTa" cols="30" rows="10">{{ $loaiBan->moTa }}</textarea>
				<label for="moTa">Mô Tả</label>
				@if($errors->has('moTa'))
				<div class="alert alert-danger">
					<strong>! {{ $errors->first('moTa') }}</strong>
				</div>
				@endif
			</div>
			<div class="text-center">
				<button class="btn btn-unique">Cập Nhật Vào CSDL
					<i class="fa fa-paper-plane-o ml-1"></i>
				</button>
			</div>
		</form>
	</div>
</div>
<!--/.Card-->

<script>
	function openPopup() {
		CKFinder.popup( {
			chooseFiles: true,
			onInit: function( finder ) {
				finder.on( 'files:choose', function( evt ) {
					var file = evt.data.files.first();
					document.getElementById( 'hinhAnh' ).value = file.getUrl().substring(file.getUrl().lastIndexOf('/') + 1);
				} );
			}
		} );				
	}

</script>
@endsection