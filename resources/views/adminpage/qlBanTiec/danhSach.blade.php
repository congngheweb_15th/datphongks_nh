@extends('adminpage.layout.layout')

@section('content')

<div class="block-header">
	<h2 class="text-uppercase">Danh Sách Các Loại Bàn Tiệc Nhà Hàng</h2>
</div>
<div class="card card-cascade narrower mt-5">
	<div class="card-body">
		@include('adminpage.layout.success')
		@include('adminpage.layout.error')
		<a class="btn btn-sm btn-success" href="{{ route('themBanTiec_get') }}">
			<i class="material-icons">add</i>
			</i> Thêm </a>
		<div class="px-4">

			<!--Table-->
			<table class="table table-hover table-responsive mb-0">

				<!--Table head-->
				<thead>
					<tr class="table-info">
						<th scope="row font-weight-bold" width="5%">#</th>
						<th class="th-md font-weight-bold text-center" width="25%">
							<a>Tên Loại Bàn
								<i class="fa fa-sort ml-1"></i>
							</a>
						</th>
						<th class="th-md font-weight-bold text-center"  width="10%">
							<a>Đơn Giá Bàn
								<i class="fa fa-sort ml-1"></i>
							</a>
						</th>
						<th class="th-md font-weight-bold  text-center" width="20%">
							<a>Ảnh Minh Họa	
							</a>
						</th>
						<th class="th-md font-weight-bold  text-center" width="30%">
							<a>Mô Tả
							</a>
						</th>
						
						<th class="th-md text-center font-weight-bold" width="5%">
							<a href="">Sửa

							</a>
						</th>
						<th class="th-md text-center font-weight-bold" width="5%">
							<a href="">Xóa

							</a>
						</th>
					</tr>
				</thead>
				<!--Table head-->

				<!--Table body-->
				<tbody>
					@foreach($loaiBan as $lb)
					<tr>
						<th scope="row" class="text-center">{{ $lb->id }}</th>
						<td class="text-center">{{ $lb->tenLoaiBan }}</td>
						<td class="text-right">{{ $lb->donGiaLoaiBan }} VND</td>
						<td class="text-center"><img src="../../public/uploads/images/{{ $lb->hinhAnhMinhHoa }}" width="100px"/></td>
						<td>{{ $lb->moTa }}</td>
						<td class="text-center">
							<a href="{{ route('suaBanTiec_get', ['id' => $lb->id]) }}">
								<img src="{{ asset('public/img/edit.svg') }}" width="24px" />
							</a>
						</td>
						<td class="text-center">
							<a href="{{ route('xoaBanTiec_get', ['id' => $lb->id]) }}">
								<img src="{{ asset('public/img/delete.svg') }}" width="24px" />
							</a>
						</td>
					</tr>
					@endforeach
				</tbody>
				<!--Table body-->
			</table>
				<div class="d-flex justify-content-center">
				{{ $loaiBan->links('adminpage.layout.custom_pagination') }}
			</div>
		</div>
	</div>
</div>

@endsection