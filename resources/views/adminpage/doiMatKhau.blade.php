@extends('adminpage.layout.layout') @section('content')
<div class="block-header">
	<h2 class="text-uppercase">Đổi Mật Khẩu</h2>
</div>
<!--Card-->
<div class="card">
	<!--Card content-->
	<div class="card-body">
		@if(session('thongbao'))
		<div class="alert alert-success">
			{{ session('thongbao') }}
		</div>
		@endif
		<form method="POST" action="{{ route('doiMatKhau_post',['id' => Auth::user()->id]) }}">
			<input type="hidden" name="_token" value="{{ csrf_token() }}" />
			<div class="md-form">
				<i class="fa fa-user prefix grey-text"></i>
				<input type="password" id="password" name="password" class="form-control">
				<label for="password">Mật Khẩu Mới</label>
				@if($errors->has('password'))
				<div class="alert alert-danger">
					<strong>! {{ $errors->first('password') }}</strong>
				</div>
				@endif
			</div>
             <div class="md-form">
				<i class="fa fa-user prefix grey-text"></i>
				<input type="text" id="password-confirm" name="password-confirm" class="form-control">
				<label for="password-confirm">Xác Nhận Mật Khẩu Mới</label>
			</div>
			<div class="text-center">
				<button class="btn btn-unique">Đổi Mật Khẩu
					<i class="fa fa-paper-plane-o ml-1"></i>
				</button>
			</div>
		</form>
	</div>

</div>
<!--/.Card-->
@endsection