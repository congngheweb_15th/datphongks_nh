@extends('DatPhong_Ban.layouts.layout')

@section('content')
<div class="container">
       <h1 class="title">GIỚI THIỆU</h1>
       <div class="row">
              <div class="col-sm-4"><br /><br /><p class="text-justify">Bằng việc xác định được mục tiêu, tâm thế rõ ràng trong suốt gần hai mươi năm phát triển, iLuzence Hotel đã luôn kiên định với triết lý kinh doanh  nhân văn gắn liền với sứ mệnh bảo tồn, giới thiệu văn hóa truyền thống độc đáo, giàu bản sắc dân tộc Việt Nam. iLuzence Hotel kỳ vọng sẽ phát triển mạnh mẽ và bền vững trong tương lai, song hành với sự phát triển của xã hội và đất nước. </p></div>
              <div class="col-sm-4">
              KHÁCH SẠN<br /><br /><p class="text-justify">iLuzence Hotel là thương hiệu khách sạn 5 sao cao cấp đã và đang mang đến một không gian nghỉ dưỡng tuyệt vời cho những kỳ nghỉ và chuyến công tác cho quý khách. Đến với iLuzence Hotel du khách được hòa mình vào không gian xinh xắn và những trải nghiệm du lịch địa phương độc đáo để thưởng thức các món ăn và phong tục vùng miền đặc sắc.</p></div>
              <div class="col-sm-4">
              NHÀ HÀNG<br /><br /><p class="text-justify">Với hệ thống phòng, sảnh cùng khuôn viên rộng được trang bị hiện đại, cùng hệ thống dịch vụ, hỗ trợ chuyên nghiệp, đầy đủ, đa dạng. iLuzence Hotel có khả năng đáp ứng tổ chức đồng thời nhiều loại hình hội nghị, hội thảo, triển lãm, biểu diễn, tiệc... có số lượng khách tham dự cùng một thời điểm lên đến hàng ngàn người, với các yêu cầu cao nhất, mang đến dịch vụ với chất lượng tốt nhất cho bạn.</p></div>              
       </div>
</div>
@endsection