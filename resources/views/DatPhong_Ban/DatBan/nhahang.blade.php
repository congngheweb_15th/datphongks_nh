@extends('DatPhong_Ban.layouts.layout') @section('content')
<div class="container">

	<h2>CÁC LOẠI BÀN ĂN, BÀN TIỆC</h2>
	<!-- form -->

	<div class="row">
        @foreach($loaiBan as $lb)
		<div class="col-sm-6 wowload fadeInUp">
			<div class="rooms">
				<img src="public/uploads/images/{{ $lb->hinhAnhMinhHoa }}" class="img-responsive" style="width:555px; height: 370px;">
				<div class="info">
					<h2>{{ $lb->tenLoaiBan }}</h2>
					<p>{{ $lb->moTa }}</p>
					<br/>
					<p id="tien" style="font-size: 30px;">Giá: {{ $lb->donGiaLoaiBan }} VND</p>
				</div>
			</div>
		</div>
        @endforeach
    </div>

</div>
@endsection