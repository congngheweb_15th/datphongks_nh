@extends('DatPhong_Ban.layouts.layout')

@section('content')
    <!-- banner -->
    <div class="banner">    	   
        <img src="{{ asset('public/img/photos/banner1.gif') }}" class="img-responsive" alt="slide">
        <div class="welcome-message">
            <div class="wrap-info">
                <div class="information">
                    <h1  class="animated fadeInDown">iLuzence Restaurant</h1>
                    <p class="animated fadeInUp">Most luxurious restaurant of VietNam with the royal treatments and excellent customer service.</p>
                </div>
                <a href="#information" class="arrow-nav scroll wowload fadeInDownBig"><i class="fa fa-angle-down"></i></a>
            </div>
        </div>
    </div>
    <!-- banner-->


    <!-- reservation-information -->
    <div id="information" class="spacer reserve-info ">
        <div class="container">
            <div class="row">
                <div class="col-sm-7 col-md-8">
                    <div><img id="hinhAnhMinhHoa" src="{{ asset('public/img/photos/vd6.jpg') }}" class="img-responsive" alt="slide"></div>
                </div>
                <div class="col-sm-5 col-md-4">
                <h3>ĐẶT BÀN</h3>
                    <form role="form" class="wowload fadeInRight" action="{{ route('datBan_post') }}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="text" class="form-control{{ $errors->has('tenKhachDat') ? ' is-invalid' : '' }}" id="tenKhachDat" name="tenKhachDat" value="{{ old('tenKhachDat') }}" placeholder="Họ tên" />
                            @if($errors->has('tenKhachDat'))
                                <div class="invalid-feedback text-danger"><strong>{{ $errors->first('tenKhachDat') }}</strong></div>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="number" class="form-control{{ $errors->has('cmnd') ? ' is-invalid' : '' }}" id="cmnd" name="cmnd" value="{{ old('cmnd') }}" placeholder="Số CMND" />
                            @if($errors->has('cmnd'))
                                <div class="invalid-feedback text-danger"><strong>{{ $errors->first('cmnd') }}</strong></div>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="number" class="form-control{{ $errors->has('sdt') ? ' is-invalid' : '' }}" id="sdt" name="sdt" value="{{ old('sdt') }}" placeholder="Số điện thoại" />
                            @if($errors->has('sdt'))
                                <div class="invalid-feedback text-danger"><strong>{{ $errors->first('sdt') }}</strong></div>
                            @endif
                        </div>      
                        <div class="form-group">
                            <input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" value="{{ old('email') }}" placeholder="Email" />
                            @if($errors->has('email'))
                                <div class="invalid-feedback text-danger"><strong>{{ $errors->first('email') }}</strong></div>
                            @endif
                        </div>  
                        <div class="form-row">
                            <div class="form-group col-md-6" >
                                <label for="ngayNhan">Ngày nhận bàn<span class="text-danger font-weight-bold">*</span></label>
                                <div class="input-group">
                                    <input type="text" class="form-control DatePicker" id="ngayNhan" name="ngayNhan" value="{{ old('ngayNhan') }}" placeholder="dd-mm-yyyy" aria-describedby="groupngayNhan" />
                                    <div class="input-group-addon" id="groupngayNhan"><i class="fa fa-calendar" aria-hidden="true"></i></div>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="gioNhan">Giờ nhận bàn <span class="text-danger font-weight-bold">*</span></label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="time" name="gioNhan" placeholder="HH:mm" value="{{ old('gioNhan') }}" />
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6" >
                                <input type="number" class="form-control{{ $errors->has('slKhach') ? ' is-invalid' : '' }}" id="slkhach" name="slKhach" value="{{ old('slKhach') }}" placeholder="Số lượng khách" />
                                @if($errors->has('slKhach'))
                                    <div class="invalid-feedback text-danger"><strong>{{ $errors->first('slKhach') }}</strong></div>
                                @endif
                            </div>
                            <div class="form-group col-md-6">
                                <input type="number" class="form-control{{ $errors->has('slBan') ? ' is-invalid' : '' }}" id="slBan" name="slBan" value="{{ old('slBan') }}" placeholder="Số lượng bàn" />
                                @if($errors->has('slBan'))
                                    <div class="invalid-feedback text-danger"><strong>{{ $errors->first('slBan') }}</strong></div>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="id_loaiBan ">Loại bàn<span class="text-danger font-weight-bold">*</span></label>
                            <select class="custom-select form-control" name="id_loaiBan" id="id_loaiBan">
                                <option value="" disabled selected>---Chọn Loại Bàn---</option>
                                @foreach($loaiBan as $lb)
                                    <option value="{{ $lb->id }}">{{ $lb->tenLoaiBan }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="tongTien">Tổng tiền:</label>
                            <span name="tien" id="tien"></span> VND
                            <input type="hidden" name="tongTien" id="tongTien" />
                        </div>
                        <div class="form-group">
                            <label for="diaDiemDatBan">Địa điểm<span class="text-danger font-weight-bold">*</span></label>
                            <select class="custom-select form-control" id="diaDiemDatBan" name="diaDiemDatBan">
                                <option value="" disabled selected>-- Chọn địa điểm --</option>
                                <option value="Ngoài trời">Ngoài trời</option>
                                <option value="Sảnh tiệc">Sảnh tiệc</option>  
                                <option value="Sân thượng">Sân thượng</option>        
                            </select>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" id="ghiChu" name="ghiChu" placeholder="Ghi chú"></textarea>
                        </div>
                        <button class="btn btn-default">Đặt bàn</button>
                    </form>    
                </div>
            </div>  
        </div>
    </div>
    <!-- reservation-information -->

@endsection
