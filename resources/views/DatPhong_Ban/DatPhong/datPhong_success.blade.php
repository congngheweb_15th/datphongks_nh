@extends('DatPhong_Ban.layouts.layout') 
@section('content')
<hr/>
<div class="container">
    <div class="panel panel-success">
        <div class="panel-heading font-weight-bold">Chúc Mừng !!!</div>
        <div class="panel-body">Bạn đã đặt phòng thành công. Vui lòng đến nhận phòng đúng ngày !!!</div>
        <div class="panel-footer"><a href="{{ route('trangChu') }}">Trở lại trang chủ</a></div>
    </div>
</div>
@endsection