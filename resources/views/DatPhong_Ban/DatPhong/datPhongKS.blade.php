@extends('DatPhong_Ban.layouts.layout') @section('content')
<!-- banner -->
<div class="banner">
	<img src="{{ asset('public/img/photos/banner.jpg') }}" class="img-responsive" alt="slide">
	<div class="welcome-message">
		<div class="wrap-info">
			<div class="information">
				<h1 class="animated fadeInDown">iLuzence Hotel</h1>
				<p class="animated fadeInUp">Most luxurious hotel of VietNam with the royal treatments and excellent customer service.</p>
			</div>
			<a href="#information" class="arrow-nav scroll wowload fadeInDownBig">
				<i class="fa fa-angle-down"></i>
			</a>
		</div>
	</div>
</div>
<!-- banner-->

<!-- reservation-information -->
<div id="information" class="spacer reserve-info ">
	<div class="container">
		<div class="row">
			<div class="col-sm-7 col-md-8">
				<div>
					<img id="hinhAnhMinhHoa" src="{{ asset('public/img/photos/9.jpg') }}" class="img-responsive" alt="slide">
				</div>
			</div>
			<div class="col-sm-5 col-md-4">
				<h3>ĐẶT PHÒNG</h3>
				<form role="form" class="wowload fadeInRight" action="{{ route('datPhong_post') }}" method="POST">
					{{ csrf_field() }}
					<div class="form-group">
						<input type="text" class="form-control{{ $errors->has('tenKhachDat') ? ' is-invalid' : '' }}" id="tenKhachDat" name="tenKhachDat"
						 value="{{ old('tenKhachDat') }}" placeholder="Họ tên" /> @if($errors->has('tenKhachDat'))
						<div class="invalid-feedback text-danger">
							<strong>{{ $errors->first('tenKhachDat') }}</strong>
						</div>
						@endif
					</div>
					<div class="form-group">
						<input type="number" class="form-control{{ $errors->has('cmnd') ? ' is-invalid' : '' }}" id="cmnd" name="cmnd" value="{{ old('cmnd') }}"
						 placeholder="Số CMND" /> @if($errors->has('cmnd'))
						<div class="invalid-feedback text-danger">
							<strong>{{ $errors->first('cmnd') }}</strong>
						</div>
						@endif
					</div>
					<div class="form-group">
						<input type="number" class="form-control{{ $errors->has('sdt') ? ' is-invalid' : '' }}" id="sdt" name="sdt" value="{{ old('sdt') }}"
						 placeholder="Số điện thoại" /> @if($errors->has('sdt'))
						<div class="invalid-feedback text-danger">
							<strong>{{ $errors->first('sdt') }}</strong>
						</div>
						@endif
					</div>
					<div class="form-group">
						<input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email" name="email" value="{{ old('email') }}"
						 placeholder="Email" /> @if($errors->has('email'))
						<div class="invalid-feedback text-danger">
							<strong>{{ $errors->first('email') }}</strong>
						</div>
						@endif
					</div>
					<div class="form-group">
						<label for="NgayNhanPhong">Ngày nhận phòng
							<span class="text-danger font-weight-bold">*</span>
						</label>
						<div class="input-group">
							<input type="text" class="form-control DatePicker" id="NgayNhanPhong" name="NgayNhanPhong" value="{{ old('NgayNhanPhong') }}"
							 placeholder="dd-mm-yyyy" aria-describedby="groupNgayNhanPhong" />
							<div class="input-group-addon" id="groupNgayNhanPhong">
								<i class="fa fa-calendar" aria-hidden="true"></i>
							</div>
						</div>
						@if($errors->has('NgayNhanPhong'))
						<div class="invalid-feedback text-danger">
							<strong>{{ $errors->first('NgayNhanPhong') }}</strong>
						</div>
						@endif
					</div>
					<div class="form-group">
						<label for="NgayTraPhong">Ngày trả phòng
							<span class="text-danger font-weight-bold">*</span>
						</label>
						<div class="input-group">
							<input type="text" class="form-control DatePicker" id="NgayTraPhong" name="NgayTraPhong" value="{{ old('NgayTraPhong') }}"
							 placeholder="dd-mm-yyyy" aria-describedby="groupNgayTraPhong" />
							<div class="input-group-addon" id="groupNgayTraPhong">
								<i class="fa fa-calendar" aria-hidden="true"></i>
							</div>
						</div>
						@if($errors->has('NgayTraPhong'))
						<div class="invalid-feedback text-danger">
							<strong>{{ $errors->first('NgayTraPhong') }}</strong>
						</div>
						@endif
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="id_loaiphong">Loại phòng
								<span class="text-danger font-weight-bold">*</span>
							</label>
							<select class="custom-select form-control" id="id_loaiphong" name="id_loaiphong">
								<option value="" disabled selected>-- Chọn loại phòng --</option>
								@foreach($loaiPhong as $lp)
								<option value="{{ $lp->id }}">{{ $lp->tenLoaiPhong }}</option>
								@endforeach
							</select>
						</div>
						<div class="form-group col-md-6">
							<label for="id_soPhong">Phòng
								<span class="text-danger font-weight-bold">*</span>
							</label>
							<select class="custom-select form-control" id="id_soPhong" name="id_soPhong">
								<option value="" disabled selected>-- Chọn phòng --</option>
							</select>
							@if($errors->has('id_soPhong'))
							<div class="invalid-feedback text-danger">
								<strong>{{ $errors->first('id_soPhong') }}</strong>
							</div>
							@endif
						</div>
					</div>
					<div class="form-group">
						<label for="tongTien">Tổng tiền:</label>
						<span id="tien" name="tien"></span> VND
						<input type="hidden" name="tongTien" id="tongTien"/>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="soNguoiLon">Số người lớn
								<span class="text-danger font-weight-bold">*</span>
							</label>
							<select class="custom-select form-control" id="soNguoiLon" name="soNguoiLon">
								<option value="" disabled selected>-- Chọn số lượng --</option>
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
							</select>
							@if($errors->has('soNguoiLon'))
							<div class="invalid-feedback text-danger">
								<strong>{{ $errors->first('soNguoiLon') }}</strong>
							</div>
							@endif
						</div>
						<div class="form-group col-md-6">
							<label for="soTreEm">Số trẻ em</label>
							<select class="custom-select form-control" id="soTreEm" name="soTreEm">
								<option value="" disabled selected>-- Chọn số lượng --</option>
								<option value="0">0</option>
								<option value="1">1</option>
								<option value="2">2</option>
							</select>
							@if($errors->has('soTreEm'))
							<div class="invalid-feedback text-danger">
								<strong>{{ $errors->first('soTreEm') }}</strong>
							</div>
							@endif
						</div>
					</div>
					<button class="btn btn-default">Đặt phòng</button>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- reservation-information -->
@endsection