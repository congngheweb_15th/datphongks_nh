@extends('DatPhong_Ban.layouts.layout') @section('content')
<div class="container">

	<h2>CÁC LOẠI PHÒNG KHÁCH SẠN</h2>
	<!-- form -->

	<div class="row">
        @foreach($loaiPhong as $lp)
		<div class="col-md-6 wowload fadeInUp">
			<div class="rooms">
				<img src="public/uploads/images/{{ $lp->hinhAnhMinhHoa }}" class="img-responsive" style="width:555px; height: 370px;">
				<div class="info">
					<h2>{{ $lp->tenLoaiPhong }}</h2>
					<p>{{ $lp->moTa }}</p>
					<br/>
					<p id="tien" style="font-size: 30px;">Giá: {{ $lp->giaLoaiPhong }} VND</p>
				</div>
			</div>
		</div>
        @endforeach
    </div>

</div>
@endsection