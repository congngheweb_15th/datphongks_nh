<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<add key="webpages:Enabled" value="true" />
	<title>iLuzence Hotel | Best hotel in VietNam</title>

	<!-- Google fonts -->
	<link href='http://fonts.googleapis.com/css?family=Raleway:300,500,800|Old+Standard+TT' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway:300,500,800' rel='stylesheet' type='text/css'>

	<!-- font awesome -->
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">


	<!-- bootstrap -->
	<link rel="stylesheet" href="{{ asset('public/css/bootstrapv3.min.css') }}" />

	<!-- uniform -->
	<link type="text/css" rel="stylesheet" href="{{ asset('public/css/uniform.default.min.css') }}" />

	<!-- animate.css -->
	<link rel="stylesheet" href="{{ asset('public/css/animate.css') }}" />

	<!-- favicon -->
	<link rel="shortcut icon" href="{{ asset('public/img/favicon.ico') }}" type="image/x-icon">
	<link rel="icon" href="{{ asset('public/img/favicon.ico') }}" type="image/x-icon">

	<link href="{{ asset('public/css/bootstrap-datepicker.min.css') }}" type="text/css" rel="stylesheet" />
	<link href="{{ asset('public/css/bootstrap-material-datetimepicker.css') }}" type="text/css" rel="stylesheet" />

	<link rel="stylesheet" href="{{ asset('public/css/datPhong_Ban_custom.css') }}" />

</head>

<body id="home">
	<!-- header -->
	<nav class="navbar navbar-expand-lg navbar-default" role="navigation">
		<div class="container">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="{{ url('/') }}">
					<img src="{{ asset('public/img/logo.png') }}" width="180px" height="70px"/>
				</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<li>
						<a href="{{ url('/') }}">TRANG CHỦ </a>
					</li>
					<li>
						<a href="{{ url('/gioithieu') }}">GIỚI THIỆU</a>
					</li>
					<li>
						<a href="{{ url('/khachsan') }}">KHÁCH SẠN</a>
					</li>
					<li>
						<a href="{{ url('/nhahang') }}">NHÀ HÀNG</a>
					</li>
				</ul>
			</div>
			<!-- Wnavbar-collapse -->
		</div>
		<!-- container-fluid -->
	</nav>
	<!-- header -->
	@yield('content')
	<hr />
	<hr />
	<footer class="spacer">
		<div class="container">
			<div class="row">
				<div class="col-sm-7">
					<h4>ILUZENCE HOTEL</h4>
					<p class="text-justify">iLuzence Hotel nằm toạ lạc tại trung tâm của thủ đô Hà Nội. Một vị trí tuyệt vời để kết nối và khám phá vẻ đẹp cổ kính
						của thành phố đã ngàn năm tuổi. Du khách có thể tản bộ tận hưởng không khí mát lành của Hồ Gươm xanh, ngắm nhà hát
						Opera trầm mặc, tận hưởng cuộc sống náo nhiệt của khu phố cổ rêu phong. Sự thuận lợi đi lại, vị trí ngay trung tâm,
						chắc chắn sẽ làm hài lòng du khách khi đến với iLuzence Hotel.
					</p>
				</div>

				<div class="col-sm-5">
					<h4>LIÊN HỆ</h4>
					18, đường Ung Văn Khiêm, phường Đông Xuyên, TP. Long Xuyên, tỉnh An Giang
					<br />
					<br />Điện thoại: (+84) 0296xxxxx
					<br />
					<br />Email: iluzencehotel@gmail.com
				</div>
			</div>
			<!--/.row-->
		</div>
		<!--/.container-->

		<!--/.footer-bottom-->
	</footer>

	<div class="text-center copyright">
		&copy;
		<script>
			document.write(new Date().getFullYear())
		</script> DH15TH - Trần Dương Hoàng Khải | Phạm Huỳnh Kim Dương | Đoàn Thanh Nhân
	</div>
	<a href="#home" class="toTop scroll">
		<i class="fa fa-angle-up"></i>
	</a>

	<script src="{{ asset('public/js/popper.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('public/js/jquery.js') }}"></script>

	<!-- wow script -->
	<script src="{{ asset('public/js/wow.min.js') }}"></script>

	<!-- uniform -->
	<script src="{{ asset('public/js/jquery.uniform.js') }}"></script>

	<!-- boostrap -->
	<script src="{{ asset('public/js/bootstrap.min.js') }}" type="text/javascript"></script>

	<!-- custom script -->
	<script src="{{ asset('public/js/datPhong_Ban_script.js') }}"></script>

	<script type="text/javascript" src="{{ asset('public/js/bootstrap-datepicker.min.js') }}"></script>
	<script type="text/javascript">
		$('.DatePicker').datepicker({
			format: "dd-mm-yyyy",
			weekStart: 1,
			startDate: "1-1-2017",
			endDate: "31-12-2040",
			startView: 2,
			maxViewMode: 2,
			clearBtn: true,
			language: "vi",
			todayHighlight: true
		});
	</script>

	<script type="text/javascript" src="{{ asset('public/js/moment.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/bootstrap-material-datetimepicker.js') }}"></script>
	<script>
		$('#time').bootstrapMaterialDatePicker({ date: false, format: 'HH:mm' });
	</script>

</body>

</html>