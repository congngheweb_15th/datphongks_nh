<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        {{-- CSS --}}
        <link href="{{ asset('public/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('public/css/mdb.min.css') }}" rel="stylesheet" type="text/css">
        <link href="{{ asset('public/css/custom.css') }}" rel="stylesheet" type="text/css">
 
    </head>
    <body>
	{{--Main Navigation--}}
    <header>
        {{--Navbar--}}
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top custom-navbar">
            <!-- Links -->
   			<ul class="navbar-nav ml-auto">
        		<li class="nav-item active">
           			<a class="nav-link font-bold custom-a btn btn-outline-white btn-md" href="{{ route('adminpage') }}">
					   {{--  <button class="btn btn-outline-white btn-sm">  --}}
					   	Đăng Nhập <span class="sr-only">(current)</span>
					   {{--  </button>  --}}
					</a>
        		</li>
        	</ul>
        </nav>
        {{--/.Navbar--}}

        {{--Mask--}}
        <div id="intro" class="view">
            <div class="container-fluid full-bg-img d-flex align-items-center justify-content-center">
            	<div class="row d-flex justify-content-center">
            		<div class="col-md-10 text-center">
						{{-- Heading --}}
						<div class="display-3 font-bold white-text mb-2">
							iLuzence 
						</div>
						{{-- Diveder --}}
						<h2 class="hr-light"></h2>

						 {{-- Descripton --}}
						 <h4 class="white-text my-4">
						 	iLuzence Hotel nằm toạ lạc tại trung tâm của thủ đô Hà Nội. Một vị trí tuyệt vời để kết nối và khám phá vẻ đẹp cổ kính
						của thành phố đã ngàn năm tuổi. Du khách có thể tản bộ tận hưởng không khí mát lành của Hồ Gươm xanh, ngắm nhà hát
						Opera trầm mặc, tận hưởng cuộc sống náo nhiệt của khu phố cổ rêu phong. Sự thuận lợi đi lại, vị trí ngay trung tâm,
						chắc chắn sẽ làm hài lòng du khách khi đến với iLuzence Hotel.
						 </h4>

						 {{-- Button --}}

						 <a href="{{ route('trangDatPhong') }}"><button type="button" class="btn btn-lg btn-warning ">Đặt Phòng Khách Sạn</button></a>
						 <a href="{{ route('trangDatBan') }}"><button type="button" class="btn btn-lg btn-danger ">Đặt Bàn Nhà Hàng</button></a>
					</div>
				 </div>
            </div>

        </div>
        {{--/.Mask
    </header>

     {{-- JS --}}
    <script type="text/javascript" src="{{ asset('public/js/jquery-3.2.1.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/popper.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('public/js/bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/mdb.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('public/js/script.js') }}"></script>

    </body>
</html>

