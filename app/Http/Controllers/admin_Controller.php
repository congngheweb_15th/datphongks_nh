<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Http\Controllers\datPhong_Controller;
use App\Http\Controllers\datBan_Controller;
class admin_Controller extends Controller
{
    public function getIndex(){
        if(Auth::check()){
            $slDatPhong = datPhong_Controller::count();
            $slDatBan = datBan_Controller::count();
            return view('adminpage.index',['sl' => $slDatPhong, 'sl2' => $slDatBan]);
        }
        else{
            return redirect()->route('dangNhap_get');
            
        }
    }
}
