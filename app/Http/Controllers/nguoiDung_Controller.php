<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Http\Controllers\datPhong_Controller;
use App\Http\Controllers\datBan_Controller;
class nguoiDung_Controller extends Controller
{
    //Danh sách người dùng
    public function getDanhSach()
    {
        $user = User::paginate(10);
        return view('adminpage.qlNguoiDung.danhSachNguoiDung', ['user' => $user]);
    }

    //Thêm 
    public function getNguoiDung_Them()
    {
        return view('adminpage.qlNguoiDung.themNguoiDung');
    }
    public function postNguoiDung_Them(Request $request)
    {
        $message = [
            'name.required' => 'Bạn chưa nhập tên tài khoản',
            'email.required' => 'Bạn chưa nhập email',
            'email.email' => 'Email không đúng định dạng',
            'email.unique' => 'Email này đã được tạo',
            'password.required' => 'Bạn chưa nhập mật khẩu',
            'password-confirm.required' => 'Bạn xác nhận lại mật khẩu',
            'phanquyen.required' => 'Bạn chưa phân quyền cho tài khoản'
        ];

        $validator = Validator::make($request->all(),
            [
                'name' => 'required',
                'email' => 'required|email|unique:tbl_users,email',
                'password' => 'required',
                'password-confirm' => 'required',
                'phanquyen' => 'required'
            ], $message);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return redirect()->route('themNguoiDung_get')->withErrors($errors);
        } else {
            $nguoiDung = new User;
            $nguoiDung->name = $request->name;
            $nguoiDung->email = $request->email;
            $nguoiDung->password = bcrypt($request->password);
            $nguoiDung->phanquyen = $request->phanquyen;
            $nguoiDung->save();

            return redirect()->route('themNguoiDung_get')->with('success', 'Đã tạo tài khoản thành công !!!');
        }

    }
    //Sửa Thể Loại
    public function getNguoiDung_Sua($id)
    {
        $nguoiDung = User::find($id);
        return view('adminpage.qlNguoiDung.suaNguoiDung', ['nguoiDung' => $nguoiDung]);
    }

    public function postNguoiDung_Sua(Request $request, $id)
    {
        $nguoiDung = User::find($id);
        $old_name = $nguoiDung->name;
        $message = [
            'name.required' => 'Bạn chưa nhập tên tài khoản',
            'phanquyen.required' => 'Bạn chưa phân quyền cho tài khoản'
        ];

        $validator = Validator::make($request->all(),
            [
                'name' => 'required',    
                'phanquyen' => 'required'
            ], $message);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return redirect()->route('suaNguoiDung_get', ['id' => $nguoiDung->id])->withErrors($errors);
        } else {
            $nguoiDung->name = $request->name;
            $nguoiDung->phanquyen = $request->phanquyen;
            $nguoiDung->save();

            return redirect()->route('suaNguoiDung_get', ['id' => $nguoiDung->id])->with('success', 'Đã sửa tài khoản ' . $old_name . ' sang ' . $request->name . ' !!!');
        }

    }

    public function getNguoiDung_Xoa($id)
    {
        $nguoiDung = User::find($id);
        $nguoiDung->delete();
        return redirect()->route('dsNguoiDung')->with('success', 'Đã xóa thành công ' . $nguoiDung->tentheloai . ' !!!');
    }

    public function getDangNhap()
    {
        return view('adminpage.dangNhap');
    }

    public function postDangNhap(Request $request)
    {
        $message = [
            'email.required' => 'Bạn chưa nhập email',
            'password.required' => 'Bạn chưa nhập mật khẩu'
        ];

        $validator = Validator::make($request->all(),
            [
                'email' => 'required',
                'password' => 'required',
            ], $message);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return redirect()->route('dangNhap_get')->withErrors($errors);
        } else {
            if( Auth::attempt(['email' => $request->email, 'password' => $request->password])){
                $slDatPhong = datPhong_Controller::count();
                $slDatBan = datBan_Controller::count();
                return view('adminpage.index',['sl' => $slDatPhong, 'sl2' => $slDatBan]);
            }
            else{
                return redirect()->route('dangNhap_get')->with(['thongbao', 'Đăng nhập không thành công. Vui lòng kiểm tra lại!!!']);
            }
        }
    }

    public function getDoiMatKhau()
    {
        return view('adminpage.doiMatKhau');
    }

    public function postDoiMatKhau(Request $request, $id)
    {
        $nguoiDung = User::find($id);
        $message = [
            'password.required' => 'Bạn chưa nhập mật khẩu mới',
            'password-confirm.required' => 'Bạn chưa xác nhận lại mật khẩu',
        ];

        $validator = Validator::make($request->all(),
            [
                'password' => 'required',
                'password-confirm' => 'required'
            ], $message);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return redirect()->route('doiMatKhau_get', ['id' => $nguoiDung->id])->withErrors($errors);
        } else {
           $nguoiDung->password = bcrypt($request->password);
           $nguoiDung->save();

           return redirect()->route('doiMatKhau_get', ['id' => $nguoiDung->id])->with(['thongbao', 'Đổi mật khẩu thành công']);
        }
    }

    public function getDangXuat()
    {
        Auth::logout();
        return redirect()->route('dangNhap_get');
    }
}
