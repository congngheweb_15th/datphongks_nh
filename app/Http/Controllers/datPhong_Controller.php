<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

use App\loaiPhong_Model;
use App\datPhong_Model;
use App\phong_Model;
use App\khachDat_Model;
class datPhong_Controller extends Controller
{
    public function getViewDatPhong(){

        $loaiPhong = loaiPhong_Model::all();

    	return view('DatPhong_Ban.DatPhong.datPhongKS', ['loaiPhong' => $loaiPhong]);
    }

    public function ajaxPhong(){
        $idLoaiPhong = Input::get('idLoaiPhong');

        $phong = phong_Model::where([['id_loaiphong','=', $idLoaiPhong], ['trangThai', '=', 0]])->get();

        return response()->json($phong);
        
    }

    public function ajaxHinhAnhMinhHoa(){
        $idLoaiPhong = Input::get('idLoaiPhong');

        $loaiPhong = loaiPhong_Model::where('id','=', $idLoaiPhong)->get();

        return response()->json($loaiPhong);
        
    }


    public function postDatPhong(Request $request){
        $message = [
            'tenKhachDat.required' => 'Bạn chưa nhập họ và tên',
            'tenKhachDat.max' => 'Bạn chỉ được nhập 255 ký tự',

            'cmnd.required' => 'Bạn chưa nhập số chứng minh nhân dân',
            'cmnd.numeric' => 'Bạn phải nhập số vào ô này',

            'sdt.required' => 'Bạn chưa nhập số điện thoại',
            'sdt.numeric' => 'Bạn phải nhập số vào ô này',
            'sdt.min' => 'Số điện thoại phải nhập tối thiểu 10 chữ số',
            'sdt.max' => 'Số điện thoại chỉ nhập tối đa 11 chữ số',

            'NgayNhanPhong.required' => 'Bạn chưa chọn ngày nhận phòng',
            'NgayTraPhong.required' => 'Bạn chưa chọn ngày trả phòng',
            'id_soPhong.required' => 'Bạn chưa chọn phòng',
            'soNguoiLon.required' => 'Bạn chưa nhập số lượng người lớn',
            'soTreEm.required' => 'Bạn chưa nhập số lượng trẻ em',
              
        ];

        $validator = Validator::make($request->all(),
            [
                'tenKhachDat' => 'required|max:255',
                'cmnd' => 'required|numeric',
                'sdt' => 'required|numeric|min:10',
                'NgayNhanPhong' => 'required',
                'NgayTraPhong' => 'required',
                'id_soPhong' => 'required',
                'soNguoiLon' => 'required',
                'soTreEm' => 'required',
      
            ], $message);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return redirect()->route('trangDatPhong')->withErrors($errors);
        } else {
            DB::beginTransaction();
            try{
                $updateTTPhong = phong_Model::where('id','=', $request->id_soPhong)->update(['trangThai'=> 1]);

                $khachDat = new khachDat_Model;
                $khachDat->hoTen = $request->tenKhachDat;
                $khachDat->cmnd = $request->cmnd;
                $khachDat->sdt = $request->sdt;
                $khachDat->email = $request->email;
                $khachDat->save();

                $datPhong = new datPhong_Model;
                $datPhong->id_khachDatPhong = $khachDat->id;
                $datPhong->ngayNhanPhong = date("Y-m-d",strtotime($request->NgayNhanPhong));
                $datPhong->ngayTraPhong = date("Y-m-d",strtotime($request->NgayTraPhong));
                $datPhong->id_soPhong = $request->id_soPhong; 
                $datPhong->soNguoiLon = $request->soNguoiLon;
                $datPhong->soTreEm = $request->soTreEm;
                $datPhong->tongTien = $request->tongTien;
                $datPhong->trangThai = 0;
                $datPhong->save();

                DB::commit();
                return redirect()->route('datPhong_success');
            }catch(Exception $e){
                DB::rollback();
                throw $e;
            }
        }
    }
   
    //--------------------------------------------------------------------------//

    public static function count(){
        $sl = ['slChoXN' => datPhong_Model::where('trangThai', '=', 0)->count(), 
        'slChoNhan' => datPhong_Model::where('trangThai', '=', 1)->count()];
        return $sl;
    }

    //Load danh sách đặt phòng theo trạng thái: 
    // 0: chưa xác nhận
    public function getDanhSachChoXN(){
        
        $DS_TT0 = datPhong_Model::where('trangThai', '=', 0)->paginate(10);
        $sl = $this->count();
        return view('adminpage.qlDatPhong.dsDatPhong', ['DS_TT0' => $DS_TT0, 'sl' => $sl]);
    }
    // 1: chờ nhận phòng
    public function getDanhSachChoNhanPhong(){
        
        $DS_TT1 = datPhong_Model::where('trangThai', '=', 1)->paginate(10);
        $sl = $this->count();
        return view('adminpage.qlDatPhong.dsChoNhanPhong', ['DS_TT1' => $DS_TT1, 'sl' => $sl]);
    }
    // 2: đã nhận phòng
    public function getDanhSachDaNhanPhong(){
        
        $DS_TT2 = datPhong_Model::where('trangThai', '=', 2)->paginate(10);
        $sl = $this->count();
        return view('adminpage.qlDatPhong.dsDaNhanPhong', ['DS_TT2' => $DS_TT2, 'sl' => $sl]);
    }
    // 3: đã thanh toán & 4: đã hủy
    public function getDanhSachDaThanhToan_Huy(){
        
        $DS_TT3 = datPhong_Model::where('trangThai', '=', 3)->paginate(10);
        $DS_TT4 = datPhong_Model::where('trangThai', '=', 4)->paginate(10);
        $sl = $this->count();
        return view('adminpage.qlDatPhong.dsDaThanhToan_Huy', ['DS_TT3' => $DS_TT3, 'DS_TT4' => $DS_TT4, 'sl' => $sl]);
    }

    //--------------------------------------------------------------------------//

    public function setTrangThai($id, $kd){
        $datPhong = datPhong_Model::find($id);

        if($kd == 1){
            $datPhong->trangThai = $kd;
            $datPhong->save();
            return redirect()->route('dsDatPhongChoNP')->with('success', 'Đã xác nhận chờ nhận phòng !!!');
        } 
        else if($kd == 2){
            $datPhong->trangThai = $kd;
            $datPhong->save();
            return redirect()->route('dsDatPhongDaNP')->with('success', 'Đã xác nhận nhận phòng thành công !!!');
        } 
        else if($kd == 3){
            $datPhong->trangThai = $kd;
            $datPhong->save();
            return redirect()->route('dsDatPhongDaTT_H')->with('success', 'Đã xác thanh toán phòng thành công !!!');
        } 
        else{
            $datPhong->trangThai = $kd;
            $datPhong->save();
            return redirect()->route('dsDatPhongDaTT_H')->with('success', 'Đã xác nhận huỷ đặt phòng thành công !!!');
        }
    }

}
