<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

use App\loaiBan_Model;
use App\khachDat_Model;
use App\datBan_Model;
class datBan_Controller extends Controller
{
    public function getViewDatBan(){

        $loaiBan = loaiBan_Model::all();

    	return view('DatPhong_Ban.DatBan.datBanNH', ['loaiBan' => $loaiBan]);
    }

    public function getAjaxHinhAnh(){
        $idloaiBan = Input::get('idLoaiBan');

        $loaiBan = loaiBan_Model::where('id','=', $idloaiBan)->get();

        return response()->json($loaiBan);
        
    }


    public function postDatBan(Request $request){
        $message = [
            'tenKhachDat.required' => 'Bạn chưa nhập họ và tên',
            'tenKhachDat.max' => 'Bạn chỉ được nhập 255 ký tự',

            'cmnd.required' => 'Bạn chưa nhập số chứng minh nhân dân',
            'cmnd.numeric' => 'Bạn phải nhập số vào ô này',

            'sdt.required' => 'Bạn chưa nhập số điện thoại',
            'sdt.numeric' => 'Bạn phải nhập số vào ô này',
            'sdt.min' => 'Số điện thoại phải nhập tối thiểu 10 chữ số',
            'sdt.max' => 'Số điện thoại chỉ nhập tối đa 11 chữ số',

            'slKhach.required' => 'Bạn chưa nhập số lượng khách',
            'slKhach.numeric' => 'Bạn phải nhập số vào ô này',
            'slBan.required' => 'Bạn chưa nhập số lượng bàn',
            'slBan.numberic' => 'Bạn phải nhập số vào ô này',
            'id_loaiBan.required' => 'Bạn chưa chọn loại bàn',
            'ngayNhan.required' => 'Bạn chưa chọn ngày nhận bàn',
            'gioNhan.required' => 'Bạn chưa nhập giờ nhận bàn',
            'diaDiemDatBan.required' => 'Bạn chưa chọn địa điểm',  
        ];

        $validator = Validator::make($request->all(),
            [
                'tenKhachDat' => 'required|max:255',
                'cmnd' => 'required|numeric',
                'sdt' => 'required|numeric|min:10',
                'slKhach' => 'required|numeric',
                'slBan' => 'required|numeric',
                'id_loaiBan' => 'required',
                'ngayNhan' => 'required',
                'gioNhan' => 'required',
                'diaDiemDatBan' => 'required',
            ], $message);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return redirect()->route('trangDatBan')->withErrors($errors);
        } else {
            DB::beginTransaction();
            try{

                $khachDat = new khachDat_Model;
                $khachDat->hoTen = $request->tenKhachDat;
                $khachDat->cmnd = $request->cmnd;
                $khachDat->sdt = $request->sdt;
                $khachDat->email = $request->email;
                $khachDat->save();

                $datBan = new datBan_Model;
                $datBan->id_loaiBan = $request->id_loaiBan;
                $datBan->id_khachDatBan  = $khachDat->id;
                $datBan->slKhach = $request->slKhach;
                $datBan->slBan = $request->slBan;
                $datBan->tongTien = $request->tongTien;
                $datBan->ngayNhan = date("Y-m-d",strtotime($request->ngayNhan));
                $datBan->gioNhan = $request->gioNhan;
                $datBan->diaDiemDatBan = $request->diaDiemDatBan;
                $datBan->ghiChu = $request->ghiChu;
                $datBan->trangThai = 0;
                $datBan->save();

                DB::commit();
                return redirect()->route('datBan_success');
            }catch(Exception $e){
                DB::rollback();
                throw $e;
            }
        }
    }

   //--------------------------------------------------------------------------//

   public static function count(){
    $sl = ['slChoXN' => datBan_Model::where('trangThai', '=', 0)->count(), 
    'slChoNhan' => datBan_Model::where('trangThai', '=', 1)->count()];
    return $sl;
}
    //Load danh sách đặt bàn theo trạng thái: 
    // 0: chưa xác nhận
    public function getDanhSachChoXN(){
        
        $DS_TT0 = datBan_Model::where('trangThai', '=', 0)->paginate(10);
        $sl = $this->count();
        return view('adminpage.qlDatBan.dsDatBan', ['DS_TT0' => $DS_TT0, 'sl' => $sl]);
    }
    // 1: chờ nhận bàn
    public function getDanhSachChoNhanBan(){
        
        $DS_TT1 = datBan_Model::where('trangThai', '=', 1)->paginate(10);
        $sl = $this->count();
        return view('adminpage.qlDatBan.dsChoNhanBan', ['DS_TT1' => $DS_TT1, 'sl' => $sl]);
    }
    // 2: đã nhận bàn
    public function getDanhSachDaNhanBan(){
        
        $DS_TT2 = datBan_Model::where('trangThai', '=', 2)->paginate(10);
        $sl = $this->count();
        return view('adminpage.qlDatBan.dsDaNhanBan', ['DS_TT2' => $DS_TT2, 'sl' => $sl]);
    }
    // 3: đã thanh toán & 4: đã hủy
    public function getDanhSachDaThanhToan_Huy(){
        
        $DS_TT3 = datBan_Model::where('trangThai', '=', 3)->paginate(10);
        $DS_TT4 = datBan_Model::where('trangThai', '=', 4)->paginate(10);
        $sl = $this->count();
        return view('adminpage.qlDatBan.dsDaThanhToan_Huy', ['DS_TT3' => $DS_TT3, 'DS_TT4' => $DS_TT4, 'sl' => $sl]);
    }

    //--------------------------------------------------------------------------//

    public function setTrangThai($id, $kd){
        $datBan = datBan_Model::find($id);

        if($kd == 1){
            $datBan->trangThai = $kd;
            $datBan->save();
            return redirect()->route('dsDatBanChoNP')->with('success', 'Đã xác nhận chờ nhận bàn !!!');
        } 
        else if($kd == 2){
            $datBan->trangThai = $kd;
            $datBan->save();
            return redirect()->route('dsDatBanDaNP')->with('success', 'Đã xác nhận nhận bàn thành công !!!');
        } 
        else if($kd == 3){
            $datBan->trangThai = $kd;
            $datBan->save();
            return redirect()->route('dsDatBanDaTT_H')->with('success', 'Đã xác nhận nhận bàn thành công !!!');
        } 
        else{
            $datBan->trangThai = $kd;
            $datBan->save();
            return redirect()->route('dsDatBanDaTT_H')->with('success', 'Đã xác nhận huỷ đặt bàn thành công !!!');
        }
    }

}
