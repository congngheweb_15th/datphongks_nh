<?php

namespace App\Http\Controllers;

use App\loaiBan_Model;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Validator;

class loaiBanTiec_Controller extends Controller
{
    //
    public function getDanhSach()
    {
        $loaiBan = loaiBan_Model::paginate(10);

        return view('adminpage.qlBanTiec.danhSach', ['loaiBan' => $loaiBan]);
    }

    public function getDanhSachLoaiBan()
    {
        $loaiBan = loaiBan_Model::all();

        return view('DatPhong_Ban.DatBan.nhahang', ['loaiBan' => $loaiBan]);
    }

    public function getThemBanTiec()
    {
        return view('adminpage.qlBanTiec.themBan');
    }

    public function postThemBanTiec(Request $request)
    {
        $message = [
            'tenLoaiBan.required' => 'Bạn chưa nhập tên loại bàn',
            'tenLoaiBan.max' => 'Bạn chỉ được nhập 255 ký tự',
            'tenLoaiBan.unique' => 'Đã có loại bàn này trong dữ liệu',
            'donGiaLoaiBan.required' => 'Bạn chưa nhập giá loại bàn',
            'donGiaLoaiBan.numeric' => 'Đơn giá loại bàn phải là số',
            'hinhAnh.required' => 'Bạn chưa chọn hình ảnh minh họa cho loại bàn',
            'moTa.required' => 'Bạn chưa nhập mô tả cho loại bàn',
        ];

        $validator = Validator::make($request->all(),
            [
                'tenLoaiBan' => 'required|max:255|unique:tbl_loaiban',
                'donGiaLoaiBan' => 'required|numeric',
                'hinhAnh' => 'required',
                'moTa' => 'required|max:255',
            ], $message);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return redirect()->route('themBanTiec_get')->withErrors($errors);
        } else {
            $loaiBan = new loaiBan_Model;
            $loaiBan->tenLoaiBan = $request->tenLoaiBan;
            $loaiBan->donGiaLoaiBan = $request->donGiaLoaiBan;
            $loaiBan->hinhAnhMinhHoa = $request->hinhAnh;
            $loaiBan->moTa = $request->moTa;
            $loaiBan->save();

            return redirect()->route('themBanTiec_get')->with('success', 'Đã thêm loại bàn "' . $request->tenLoaiBan . '"!!!');
        }

    }

    public function getSuaBanTiec($id)
    {
        $loaiBan = loaiBan_Model::find($id);
        return view('adminpage.qlBanTiec.suaBanTiec', ['loaiBan' => $loaiBan]);
    }

    public function postSuaBanTiec(Request $request, $id)
    {

        $loaiBan = loaiBan_Model::find($id);
        $message = [
            'tenLoaiBan.required' => 'Bạn chưa nhập tên loại bàn',
            'tenLoaiBan.max' => 'Bạn chỉ được nhập 255 ký tự',
            'donGiaLoaiBan.required' => 'Bạn chưa nhập giá loại bàn',
            'donGiaLoaiBan.numeric' => 'Đơn giá loại bàn phải là số',
            'hinhAnh.required' => 'Bạn chưa chọn hình ảnh minh họa cho loại bàn',
            'moTa.required' => 'Bạn chưa nhập mô tả cho loại bàn',
        ];

        $validator = Validator::make($request->all(),
            [
                'tenLoaiBan' => 'required|max:255',
                'donGiaLoaiBan' => 'required|numeric',
                'hinhAnh' => 'required',
                'moTa' => 'required|max:255',
            ], $message);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return redirect()->route('suaBanTiec_get')->withErrors($errors);
        } else {
            $loaiBan->tenLoaiBan = $request->tenLoaiBan;
            $loaiBan->donGiaLoaiBan = $request->donGiaLoaiBan;
            $loaiBan->hinhAnhMinhHoa = $request->hinhAnh;
            $loaiBan->moTa = $request->moTa;
            $loaiBan->save();

            return redirect()->route('suaBanTiec_get', ['id' => $loaiBan->id])->with('success', 'Đã cập nhật thành công thông tin loại bàn!!!');
        }

    }

    public function getXoaBanTiec($id)
    {
        try {
            $loaiBan = loaiBan_Model::find($id);
            $tmp = $loaiBan->tenLoaiBan;
            $loaiBan->delete();
            return redirect()->route('danhSachBanTiec')->with('success', 'Đã xóa thành công loại bàn "' . $tmp . '"');
        } catch (QueryException $e) {
            $maLoi = $e->errorInfo[1];
            if ($maLoi === 1451) {
                return redirect()->route('danhSachBanTiec')->with('error', 'Không thể xóa loại bàn "' . $tmp . '"');
            }
        }

    }
}
