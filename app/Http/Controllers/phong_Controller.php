<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Validator;

use App\phong_Model;
use App\loaiPhong_Model;
class phong_Controller extends Controller
{
    //
    public function getDanhSach()
    {
        $phong = phong_Model::paginate(10);
        return view('adminpage.qlPhong.danhSach', ['phong' => $phong]);
    }

    public function getThemPhong()
    {
        $loaiPhong = loaiPhong_Model::all();
        return view('adminpage.qlPhong.themPhong', ['loaiPhong' => $loaiPhong]);
    }

    public function postThemPhong(Request $request)
    {
        $message = [
            'soPhong.required' => 'Bạn chưa nhập số phòng',
            'soPhong.max' => 'Bạn chỉ được nhập tối đa 5 ký tự',
            'soPhong.unique' => 'Đã có phòng này trong dữ liệu',
            'loaiPhong.required' => 'Bạn chưa chọn loại phòng',
        ];

        $validator = Validator::make($request->all(),
            [
                'soPhong' => 'required|max:5|unique:tbl_phong',
                'loaiPhong' => 'required',
            
            ], $message);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return redirect()->route('themPhong_get')->withErrors($errors);
        } else {
            DB:
            $phong = new phong_Model;
            $phong->soPhong = $request->soPhong;
            $phong->id_loaiPhong = $request->loaiPhong;
            $phong->trangThai = 0;
            $phong->save();
            
            return redirect()->route('themPhong_get')->with('success', 'Đã thêm phòng "' . $request->soPhong . '"!!!');
        }

    }

    public function getSuaPhong($id)
    {
        $phong = phong_Model::find($id);
        $loaiPhong = loaiPhong_Model::all();
        return view('adminpage.qlPhong.suaPhong', ['phong' => $phong, 'loaiPhong' => $loaiPhong]);
    }

    public function postSuaPhong(Request $request, $id)
    {

        $phong = phong_Model::find($id);
        $loaiPhong = loaiPhong_Model::all();
        $message = [
            'soPhong.required' => 'Bạn chưa nhập số phòng',
            'soPhong.max' => 'Bạn chỉ được nhập tối đa 5 ký tự',
            'soPhong.unique' => 'Đã có phòng này trong dữ liệu',
            'loaiPhong.required' => 'Bạn chưa chọn loại phòng',
        ];

        $validator = Validator::make($request->all(),
            [
                'soPhong' => 'required|max:5|unique:tbl_phong',
                'loaiPhong' => 'required',
            ], $message);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return redirect()->route('suaPhong_get')->withErrors($errors);
        } else {
            $phong->soPhong = $request->soPhong;
            $phong->id_loaiPhong = $request->loaiPhong;
            $phong->trangThai = 0;
            $phong->save();

            return redirect()->route('suaPhong_get', ['id' => $phong->id, 'loaiPhong' => $loaiPhong])->with('success', 'Đã cập nhật thành công thông tin phòng!!!');
        }

    }
    public function getXoaPhong($id)
    {
        try {
            $phong = phong_Model::find($id);
            $tmp = $phong->soPhong;
            $phong->delete();
            return redirect()->route('danhSachPhong')->with('success', 'Đã xóa thành công phòng "'.$tmp.'"');
        } catch (QueryException $e) {
            $maLoi = $e->errorInfo[1];
            if($maLoi === 1451){
                return redirect()->route('danhSachPhong')->with('error', 'Không thể xóa phòng "' . $tmp . '"');
            }
        }

    }

}
