<?php

namespace App\Http\Controllers;

use App\loaiPhong_Model;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Validator;

class loaiPhong_Controller extends Controller
{
    //

    public function getDanhSach()
    {
        $loaiPhong = loaiPhong_Model::paginate(10);

        return view('adminpage.qlLoaiPhong.danhSach', ['loaiPhong' => $loaiPhong]);
    }

    public function getDanhSachLoaiPhong()
    {
        $loaiPhong = loaiPhong_Model::all();

        return view('DatPhong_Ban.DatPhong.khachsan', ['loaiPhong' => $loaiPhong]);
    }
    
    public function getThemLoaiPhong()
    {
        return view('adminpage.qlLoaiPhong.themLoaiPhong');
    }

    public function postThemLoaiPhong(Request $request)
    {
        $message = [
            'tenLoaiPhong.required' => 'Bạn chưa nhập tên loại phòng',
            'tenLoaiPhong.max' => 'Bạn chỉ được nhập 255 ký tự',
            'tenLoaiPhong.unique' => 'Đã có loại phòng này trong dữ liệu',
            'giaLoaiPhong.required' => 'Bạn chưa nhập giá loại phòng',
            'giaLoaiPhong.numeric' => 'Giá loại phòng phải là số',
            'hinhAnh.required' => 'Bạn chưa chọn hình ảnh minh họa cho loại phòng',
            'moTa.required' => 'Bạn chưa nhập mô tả cho loại phòng',
        ];

        $validator = Validator::make($request->all(),
            [
                'tenLoaiPhong' => 'required|max:255|unique:tbl_loaiphong',
                'giaLoaiPhong' => 'required|numeric',
                'hinhAnh' => 'required',
                'moTa' => 'required|max:255',
            ], $message);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return redirect()->route('themLoaiPhong_get')->withErrors($errors);
        } else {
            $loaiPhong = new loaiPhong_Model;
            $loaiPhong->tenLoaiPhong = $request->tenLoaiPhong;
            $loaiPhong->giaLoaiPhong = $request->giaLoaiPhong;
            $loaiPhong->hinhAnhMinhHoa = $request->hinhAnh;
            $loaiPhong->moTa = $request->moTa;
            $loaiPhong->save();

            return redirect()->route('themLoaiPhong_get')->with('success', 'Đã thêm loại phòng "' . $request->tenLoaiPhong . '"!!!');
        }

    }

    public function getSuaLoaiPhong($id)
    {
        $loaiPhong = loaiPhong_Model::find($id);
        return view('adminpage.qlLoaiPhong.suaLoaiPhong', ['loaiPhong' => $loaiPhong]);
    }

    public function postSuaLoaiPhong(Request $request, $id)
    {

        $loaiPhong = loaiPhong_Model::find($id);
        $message = [
            'tenLoaiPhong.required' => 'Bạn chưa nhập tên loại phòng',
            'tenLoaiPhong.max' => 'Bạn chỉ được nhập 255 ký tự',
            'giaLoaiPhong.required' => 'Bạn chưa nhập giá loại phòng',
            'giaLoaiPhong.numeric' => 'Giá loại phòng phải là số',
            'hinhAnh.required' => 'Bạn chưa chọn hình ảnh minh họa cho loại phòng',
            'moTa.required' => 'Bạn chưa nhập mô tả cho loại phòng',
        ];

        $validator = Validator::make($request->all(),
            [
                'tenLoaiPhong' => 'required|max:255',
                'giaLoaiPhong' => 'required|numeric',
                'hinhAnh' => 'required',
                'moTa' => 'required|max:255',
            ], $message);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return redirect()->route('suaLoaiPhong_get')->withErrors($errors);
        } else {
            $loaiPhong->tenLoaiPhong = $request->tenLoaiPhong;
            $loaiPhong->giaLoaiPhong = $request->giaLoaiPhong;
            $loaiPhong->hinhAnhMinhHoa = $request->hinhAnh;
            $loaiPhong->moTa = $request->moTa;
            $loaiPhong->save();

            return redirect()->route('suaLoaiPhong_get', ['id' => $loaiPhong->id])->with('success', 'Đã cập nhật thành công thông tin loại phòng!!!');
        }

    }

    public function getXoaLoaiPhong($id)
    {
        try {
            $loaiPhong = loaiPhong_Model::find($id);
            $tmp = $loaiPhong->tenLoaiPhong;
            $loaiPhong->delete();
            return redirect()->route('danhSachLoaiPhong')->with('success', 'Đã xóa thành công loại phòng "'.$tmp.'"');
        } catch (QueryException $e) {
            $maLoi = $e->errorInfo[1];
            if($maLoi === 1451){
                return redirect()->route('danhSachLoaiPhong')->with('error', 'Không thể xóa loại phòng "' . $tmp . '"');
            }
        }

    }
}
