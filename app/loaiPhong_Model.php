<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class loaiPhong_Model extends Model
{
    //
    protected $table = 'tbl_loaiphong';

    //

    public function phong()
    {
        return $this->hasMany('App\phong_Model','id_loaiphong','id');
    }
}
