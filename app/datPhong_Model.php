<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class datPhong_Model extends Model
{
    //
    protected $table = 'tbl_datphong';

    //
    public function phong()
    {
        return $this->belongsTo('App\phong_Model','id_soPhong','id');
    }

    public function khachdat()
    {
        return $this->belongsTo('App\khachDat_Model','id_khachDatPhong','id');
    }
}
