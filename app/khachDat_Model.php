<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class khachDat_Model extends Model
{
    //
    protected $table = 'tbl_khachdat';

    //
    public function datphong()
    {
        return $this->hasMany('App\datPhong_Model','id_khachDatPhong','id');
    }

    public function datban()
    {
        return $this->hasMany('App\datBan_Model','id_khachDatBan','id');
    }
}
