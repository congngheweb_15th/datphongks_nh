<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class datBan_Model extends Model
{
   //
   protected $table = 'tbl_datban';
   
       //
       public function loaiban()
       {
           return $this->belongsTo('App\loaiBan_Model','id_loaiBan','id');
       }
   
       public function khachdat()
       {
           return $this->belongsTo('App\khachDat_Model','id_khachDatBan','id');
       }
}
