<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class phong_Model extends Model
{
    //
    protected $table = 'tbl_phong';

    //
    public function loaiphong()
    {
        return $this->belongsTo('App\loaiPhong_Model','id_loaiphong','id');
    }

    public function datphong()
    {
        return $this->hasMany('App\datPhong_Model','id_soPhong','id');
    }
}
